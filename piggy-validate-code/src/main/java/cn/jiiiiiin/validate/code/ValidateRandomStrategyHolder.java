/**
 *
 */
package cn.jiiiiiin.validate.code;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 校验码随机生成器
 *
 * @author zhailiang
 *
 */
@Component
@AllArgsConstructor
public class ValidateRandomStrategyHolder {

  private static final String VALIDATERANDOMSTRATEGY_CLASSNAME_SEPARATOR = ValidateRandomStrategy.class
      .getSimpleName();

  private final Map<String, ? extends ValidateRandomStrategy> validateRandomStrategyMap;

  /**
   * @param type {@link ValidateRandomStrategy.Type}
   * @return {@link ValidateRandomStrategy}
   */
  public ValidateRandomStrategy findValidateRandomStrategy(ValidateRandomStrategy.Type type) {
    return findValidateRandomStrategy(type.toString().toLowerCase());
  }

  /**
   * @param type {@link ValidateRandomStrategy.Type}
   * @return {@link ValidateRandomStrategy}
   */
  public ValidateRandomStrategy findValidateRandomStrategy(String type) {
    val name = StringUtils.lowerCase(String.valueOf(type.charAt(0))).concat(type.substring(1))
        .concat(VALIDATERANDOMSTRATEGY_CLASSNAME_SEPARATOR);
    ValidateRandomStrategy strategy = validateRandomStrategyMap.get(name);
    if (strategy == null) {
      throw new ValidateCodeException("校验码随机生成器" + name + "不存在");
    }
    return strategy;
  }

}

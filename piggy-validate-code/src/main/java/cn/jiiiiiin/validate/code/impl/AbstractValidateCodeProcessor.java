/**
 *
 */
package cn.jiiiiiin.validate.code.impl;

import cn.jiiiiiin.validate.code.ValidateCodeException;
import cn.jiiiiiin.validate.code.ValidateCodeGenerator;
import cn.jiiiiiin.validate.code.ValidateCodeProcessor;
import cn.jiiiiiin.validate.code.ValidateCodeRepository;
import cn.jiiiiiin.validate.code.ValidateCodeType;
import cn.jiiiiiin.validate.code.entity.ValidateCode;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 抽象的图片验证码处理器
 *
 * @author zhailiang
 */
@Slf4j
public abstract class AbstractValidateCodeProcessor<C extends ValidateCode> implements
    ValidateCodeProcessor {

  public static final String VALIDATECODEPROCESSOR_CLASSNAME_SEPARATOR = ValidateCodeProcessor.class
      .getSimpleName();

  /**
   * 收集系统中所有的 {@link ValidateCodeGenerator} 接口的实现。
   */
  @Autowired
  private Map<String, ValidateCodeGenerator> validateCodeGenerators;

  @Autowired
  private ValidateCodeRepository validateCodeRepository;

  @Override
  public void create(ServletWebRequest request) throws Exception {
    C validateCode = generate(request);
    save(request, validateCode);
    send(request, validateCode);
  }

  /**
   * 生成校验码
   *
   * @param request
   * @return
   */
  @SuppressWarnings("unchecked")
  private C generate(ServletWebRequest request) {
    String type = getValidateCodeType(request).toString().toLowerCase();
    String generatorName = type + ValidateCodeGenerator.class.getSimpleName();
    ValidateCodeGenerator validateCodeGenerator = validateCodeGenerators.get(generatorName);
    if (validateCodeGenerator == null) {
      throw new ValidateCodeException("验证码生成器" + generatorName + "不存在");
    }
    return (C) validateCodeGenerator.generate(request);
  }

  /**
   * 保存校验码
   * <p>
   * 开启spring session -》 redis之后，就不能直接把image code放到session中，避免出现错误，{@link cn.jiiiiiin.validate.code.image.ImageCode}
   *
   * @param request
   * @param validateCode
   */
  private void save(ServletWebRequest request, C validateCode) {
    // 注意：这里需要重新构建一个支持redis存储的`ValidateCode`，因为`ImageCode#Captcha`这种字段无法被序列化，`Caused by: com.fasterxml.jackson.databind.exc.InvalidDefinitionException: No serializer found for class sun.lwawt.macosx.LWCToolkit$OSXPlatformFont and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: cn.jiiiiiin.security.core.validate.code.image.ImageCode["captcha"]->com.wf.captcha.GifCaptcha["font"]->java.awt.Font["peer"])`
    val code = new ValidateCode(validateCode.getCode(), validateCode.getOriginExpireSecondsTime());
    //  因为使用 token 模式进行认证是没有 session 的，故之前将  验证码存储在 session 中的做法就并不可行，故思路就会改成上面的方式：
    //
    //  1.在请求验证码的时候，传递一个`deviceId`标识客户端
    //
    //  2.将验证码和标识存储到类 redis 存储中；
    //
    //  3.在校验的时候获取存储的数据进行校验
//     sessionStrategy.setAttribute(request, SESSION_KEY_VALIDATE_CODE, code);
    validateCodeRepository.save(request, code, getValidateCodeType(request));
  }

  /**
   * 发送校验码，由子类实现
   *
   * @param request {@link ServletWebRequest}
   * @param validateCode {@link ValidateCode}
   * @throws Exception 发送错误
   */
  protected abstract void send(ServletWebRequest request, C validateCode) throws Exception;

  /**
   * 根据请求的url获取校验码的类型
   *
   * @param request {@link ServletWebRequest}
   * @return {@link ValidateCodeType}
   */
  private ValidateCodeType getValidateCodeType(ServletWebRequest request) {
    String type = StringUtils
        .substringBefore(getClass().getSimpleName(), VALIDATECODEPROCESSOR_CLASSNAME_SEPARATOR);
    return ValidateCodeType.valueOf(type.toUpperCase());
  }

  /**
   * @param request {@link ServletWebRequest}
   */
  @SuppressWarnings("unchecked")
  @Override
  public void validate(ServletWebRequest request) {

    final ValidateCodeType codeType = getValidateCodeType(request);

    final C cacheRealValidateCode = (C) validateCodeRepository.get(request, codeType);

    if (cacheRealValidateCode == null) {
      throw new ValidateCodeException("验证码不存在，或已经过期，请重试");
    }

    // 请求传递过来的验证码
    String validateCode;
    try {
      validateCode = ServletRequestUtils.getStringParameter(request.getRequest(),
          codeType.getParamNameOnValidate());
    } catch (ServletRequestBindingException e) {
      throw new ValidateCodeException("获取验证码的值失败");
    }

    if (StringUtils.isBlank(validateCode)) {
      throw new ValidateCodeException("验证码不能为空");
    }
    if (ValidateCode.isExpired(cacheRealValidateCode.getExpireTime())) {
      validateCodeRepository.remove(request, codeType);
      throw new ValidateCodeException("验证码已经过期");
    }
    if (!StringUtils.equalsIgnoreCase(cacheRealValidateCode.getCode(), validateCode)) {
      // TODO 没能抛出到前端， 没有启动ac服务的时候
      throw new ValidateCodeException("验证码不匹配，或已经过期，请重试");
    }
    // 验证通过
    validateCodeRepository.remove(request, codeType);

  }

}

package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.image.ImageValidateCodeGenerator;
import cn.jiiiiiin.validate.code.impl.DefaultValidateCodeCheckFailureHandler;
import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import cn.jiiiiiin.validate.code.sms.DefaultSmsCodeSender;
import cn.jiiiiiin.validate.code.sms.SmsCodeSender;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiiiiiin
 */
@Configuration
@AllArgsConstructor
public class ValidateCodeBeanConfig {

  private final ValidateCodeProperties validateCodeProperties;

  @Bean
  @ConditionalOnMissingClass
  public ValidateCodeCheckFailureHandler validateCodeFilterFailureHandler() {
    return new DefaultValidateCodeCheckFailureHandler();
  }

  @Bean
  @ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
  public ValidateCodeGenerator imageValidateCodeGenerator() {
    return new ImageValidateCodeGenerator(validateCodeProperties);
  }

  @Bean
  @ConditionalOnMissingBean(name = "smsCodeSender")
  public SmsCodeSender smsCodeSender() {
    return new DefaultSmsCodeSender();
  }


}

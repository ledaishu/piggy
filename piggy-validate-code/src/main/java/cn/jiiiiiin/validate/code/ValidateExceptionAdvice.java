package cn.jiiiiiin.validate.code;


import cn.jiiiiiin.mvc.common.exception.ApiErrorCode;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import cn.jiiiiiin.mvc.common.api.R;

/**
 * 异常处理
 *
 * @author jiiiiiin
 */
@RestControllerAdvice
@Slf4j
@Order(1)
public class ValidateExceptionAdvice {

  @ExceptionHandler(value = ValidateCodeException.class)
  public R<ValidateCodeException> handlerException(HttpServletRequest req,
      ValidateCodeException ex) {
    log.error("ValidateExceptionAdvice异常处理器被执行 [ValidateCodeException]", ex);
    return restResult(ex, ApiErrorCode.FAILED.getCode(), ex.getMessage());
  }

  private static <T> R<T> restResult(T data, long code, String msg) {
    R<T> apiResult = new R<>();
    apiResult.setCode(code);
    apiResult.setData(data);
    apiResult.setMsg(msg);
    return apiResult;
  }

}

package cn.jiiiiiin.validate.code.config;

import cn.jiiiiiin.validate.code.ValidateCodeRepository;
import cn.jiiiiiin.validate.code.impl.RedisValidateCodeRepository;
import cn.jiiiiiin.validate.code.impl.SessionValidateCodeRepository;
import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiiiiiin
 */
@Configuration
@EnableConfigurationProperties(ValidateCodeProperties.class)
public class ValidateCodeConfig {

  /**
   * 如果出现上面错误就是缺少指定的验证码存储器，建议如果应用只部署一个实例，那么就选择基于session的存储方案，简单，`SessionValidateCodeRepository`，如果部署多实例且不想做session黏贴就使用redis存储方案，`RedisValidateCodeRepository`；
   *
   * @return {@link ValidateCodeRepository}
   */
  @Bean
  @ConditionalOnMissingBean(ValidateCodeRepository.class)
  public ValidateCodeRepository validateCodeRepository() {
    return new SessionValidateCodeRepository();
  }

}

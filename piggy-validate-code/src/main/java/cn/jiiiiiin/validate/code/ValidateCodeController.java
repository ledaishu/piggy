package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.dict.ValidateCodeDict;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 校验码提供控制器
 * <p>
 * 流程： ![](https://ws1.sinaimg.cn/large/0069RVTdgy1fupfn8ecd6j30yv0fiwfo.jpg)
 *
 * @author jiiiiiin
 */
@RestController
@AllArgsConstructor
@Slf4j
@Validated
public class ValidateCodeController {

  private final ValidateCodeProcessorHolder validateCodeProcessorHolder;

  /**
   * 创建验证码，根据验证码类型不同，调用不同的 {@link ValidateCodeProcessor}接口实现
   * <p>
   * `@GetMapping(SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/{type:image|sms}")`
   * 这种方式进行验证，如果请求path variable不合法将会报404，故使用下面的方式要优雅一些
   *
   * @param request {@link HttpServletRequest}
   * @param response {@link HttpServletRequest}
   * @param type 验证码类型标示
   * @throws Exception 验证码生成错误
   */
  @GetMapping(ValidateCodeDict.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/{type}")
  public void createCode(HttpServletRequest request, HttpServletResponse response,
      @PathVariable @Pattern(regexp = "image|sms", message = "验证码类型不匹配错误，目前只支持[image|sms]两类验证码") String type)
      throws Exception {
    validateCodeProcessorHolder.findValidateCodeProcessor(type)
        .create(new ServletWebRequest(request, response));
  }

}

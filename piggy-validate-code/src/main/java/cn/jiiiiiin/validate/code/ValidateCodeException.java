package cn.jiiiiiin.validate.code;

//import org.springframework.security.core.AuthenticationException;

/**
 * `AuthenticationException`是框架身份认证处理异常的基类
 *
 * @author jiiiiiin
 */
//public class ValidateCodeException extends AuthenticationException {
public class ValidateCodeException extends RuntimeException {

    public ValidateCodeException(String msg) {
        super(msg);
    }

    public ValidateCodeException(String msg, Throwable e) {
        super(msg, e);
    }
}

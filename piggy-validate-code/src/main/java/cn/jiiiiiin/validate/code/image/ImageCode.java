package cn.jiiiiiin.validate.code.image;

import cn.jiiiiiin.validate.code.entity.ValidateCode;
import com.wf.captcha.base.Captcha;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jiiiiiin
 */
@Getter
@Setter
public class ImageCode extends ValidateCode {

  private static final long serialVersionUID = -5743733411744551242L;
  /**
   * 根据验证码生成的图片
   */
  private Captcha captcha;

  /**
   * 验证码显示内容，一般等同{@link ValidateCode#code}，但是如果需要显示如，`***xxx***`这样的内容，那么code是用来存储真实的`xxx`而这里这个字段是用来存储显示内容
   */
  private String showCode;

  /**
   *
   * @param code {@inheritDoc}
   * @param captcha {@inheritDoc}
   * @param expireIn 多少秒后过期
   */
  public ImageCode(String code, Captcha captcha, int expireIn) {
    super(code, expireIn);
    this.captcha = captcha;
  }
}

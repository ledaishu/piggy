package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.entity.ValidateCode;

/**
 * 验证码随机策略
 *
 * @author jiiiiiin
 */
public interface ValidateRandomStrategy<T extends ValidateCode> {

  enum Type {
    Split
  }

  /**
   * 1.生成随机验证码，需要根据{@link ValidateCode#length}长度来生成，一般在生成验证码的时候需要根据`length`生成，但是可能存在`***`这样的标记，故这里去showCode长度
   * <p>
   * 2.设置验证码`code`
   * <p>
   * 3.设置显示验证码`showCode`[可选]
   *
   * @param validateCode 验证码对象 {@link ValidateCode}
   * @param originStr 要显示的原字符串
   */
  default void generatorCode(T validateCode, String originStr) {
  }

}

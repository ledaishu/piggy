/**
 *
 */
package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.entity.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 校验码存取器
 *
 * @author zhailiang
 */
public interface ValidateCodeRepository {

  /**
   * 保存验证码
   *
   * @param request {@link ServletWebRequest}
   * @param code {@link ValidateCode}
   * @param validateCodeType {@link ValidateCodeType}
   */
  void save(ServletWebRequest request, ValidateCode code, ValidateCodeType validateCodeType);

  /**
   * 获取验证码
   *
   * @param request {@link ServletWebRequest}
   * @param validateCodeType {@link ValidateCodeType}
   * @return {@link ValidateCode}
   */
  ValidateCode get(ServletWebRequest request, ValidateCodeType validateCodeType);

  /**
   * 移除验证码
   *
   * @param request {@link ServletWebRequest}
   * @param codeType {@link ValidateCodeType}
   */
  void remove(ServletWebRequest request, ValidateCodeType codeType);

}

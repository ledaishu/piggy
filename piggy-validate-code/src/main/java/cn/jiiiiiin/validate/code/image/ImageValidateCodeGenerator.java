package cn.jiiiiiin.validate.code.image;

import cn.hutool.core.util.ReflectUtil;
import cn.jiiiiiin.validate.code.ValidateCodeException;
import cn.jiiiiiin.validate.code.ValidateCodeGenerator;
import cn.jiiiiiin.validate.code.dict.ValidateCodeDict;
import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import com.wf.captcha.base.Captcha;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 默认的图形验证码生成器
 *
 * @author jiiiiiin
 */
@Slf4j
public class ImageValidateCodeGenerator implements ValidateCodeGenerator {

  protected final ValidateCodeProperties validateCodeProperties;

  public ImageValidateCodeGenerator(ValidateCodeProperties validateCodeProperties) {
    this.validateCodeProperties = validateCodeProperties;
  }

  @Override
  public ImageCode generate(ServletWebRequest request) {
    ImageCode imageCode;
    val expireIn = ServletRequestUtils
        .getIntParameter(request.getRequest(), ValidateCodeDict.DEFAULT_PARAMETER_NAME_EXPIRE_IN,
            validateCodeProperties.getImageCode().getExpireIn());

    var width = ServletRequestUtils.getIntParameter(request.getRequest(), "width",
        Integer.parseInt(validateCodeProperties.getImageCode().getWidth()));
    var height = ServletRequestUtils.getIntParameter(request.getRequest(), "height",
        Integer.parseInt(validateCodeProperties.getImageCode().getHeight()));
    var length = ServletRequestUtils.getIntParameter(request.getRequest(), "length",
        Integer.parseInt(validateCodeProperties.getImageCode().getLength()));
    val typeParams = ServletRequestUtils.getStringParameter(request.getRequest(), "type",
        validateCodeProperties.getImageCode().getCodeImgType().name());

    Captcha captcha;

    try {
      captcha = (Captcha) ReflectUtil
          .newInstance(Class.forName("com.wf.captcha." + typeParams), width, height, length);
    } catch (ClassNotFoundException e) {
      throw new ValidateCodeException("图形验证码生成器类型配置不正确", e);
    }
    // 获取验证码的字符
    val capText = captcha.text();
    imageCode = new ImageCode(capText, captcha, expireIn);
    log.info("IMG_CODE 图形验证码 {}：【 {} 】 码、有效期： 【 {} 】秒", typeParams, capText, expireIn);
    return imageCode;
  }
}

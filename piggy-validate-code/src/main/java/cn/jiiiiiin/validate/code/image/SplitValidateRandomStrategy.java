package cn.jiiiiiin.validate.code.image;

import cn.jiiiiiin.validate.code.ValidateCodeException;
import cn.jiiiiiin.validate.code.ValidateRandomStrategy;
import cn.jiiiiiin.validate.code.entity.ValidateCode;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 随机截取生成器
 *
 * @author: zhaojin
 **/
@Component
@Slf4j
public class SplitValidateRandomStrategy implements ValidateRandomStrategy<ImageCode> {

  @Override
  public void generatorCode(ImageCode imageCode, String originStr) {
    if (StringUtils.isBlank(originStr)) {
      throw new ValidateCodeException("待随机的字符串为空错误");
    }
    val randomLen = imageCode.getCaptcha().getLen();
    val code = _random(originStr, randomLen);
    imageCode.setCode(code);
    imageCode.setShowCode(_createShowCode(originStr, code));
  }

  private String _random(String originStr, int randomLen) {
    int codeLength = originStr.length();
    if ((codeLength - randomLen) <= 0) {
      throw new ValidateCodeException("请求的验证码超长");
    }
    // 获取随机要截取的字符串的开始下标
    int startIndex = RandomUtils.nextInt(0, codeLength - randomLen + 1);
    // 截取需要的位数
    int endtIndex = startIndex + randomLen;
    return StringUtils.substring(originStr, startIndex, endtIndex);
  }

  public String _createShowCode(String original, String code) {
    val matchStrs = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c",
        "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
        "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    val replaceStrs = new String[]{"*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*",
        "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*",
        "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*",
        "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*"};
    // 把身份证号或者手机号全部替换为*字符串
    String replaceStr = StringUtils.replaceEach(original, matchStrs, replaceStrs);
    int codeIndex = original.indexOf(code);
    var showCode = StringUtils.substring(replaceStr, 0, codeIndex) + code
        + StringUtils.substring(replaceStr, codeIndex + code.length());
    return showCode;
  }
}

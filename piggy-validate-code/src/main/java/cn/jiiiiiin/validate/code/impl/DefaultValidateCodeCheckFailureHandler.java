package cn.jiiiiiin.validate.code.impl;

import cn.jiiiiiin.validate.code.ValidateCodeException;
import cn.jiiiiiin.validate.code.ValidateCodeCheckFailureHandler;
import cn.jiiiiiin.validate.code.dict.ValidateCodeDict;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 * @author jiiiiiin
 */
@Slf4j
public class DefaultValidateCodeCheckFailureHandler implements ValidateCodeCheckFailureHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onValidateFailure(HttpServletRequest request, HttpServletResponse response, ValidateCodeException exception) throws IOException {
        log.error("验证码校验失败", exception);
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setContentType(ValidateCodeDict.CONTENT_TYPE_JSON);
        response.getWriter().write(objectMapper.writeValueAsString(exception.getMessage()));
        // TODO java.lang.IllegalStateException: UT010005: Cannot call getOutputStream(), getWriter() already called
        response.getWriter().flush();
        response.getWriter().close();
    }
}

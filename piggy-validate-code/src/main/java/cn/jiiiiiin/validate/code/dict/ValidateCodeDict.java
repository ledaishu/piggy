package cn.jiiiiiin.validate.code.dict;

/**
 * @author jiiiiiin
 */
public interface ValidateCodeDict {

  String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";

  /**
   * 验证图片验证码时，http请求中默认的携带图片验证码信息的参数的名称
   */
  String DEFAULT_PARAMETER_NAME_CODE_IMAGE = "imageCode";
  /**
   * 验证短信验证码时，http请求中默认的携带短信验证码信息的参数的名称
   */
  String DEFAULT_PARAMETER_NAME_CODE_SMS = "smsCode";

  /**
   * 默认的处理验证码的url前缀
   */
  String DEFAULT_VALIDATE_CODE_URL_PREFIX = "/code";

  /**
   * 发送短信验证码 或 验证短信验证码时，传递手机号的参数的名称
   */
  String DEFAULT_PARAMETER_NAME_MOBILE = "mobile";
  /**
   * 请求验证码(token认证模式)时候需要的客户端标识
   * @see cn.jiiiiiin.validate.code.ValidateCodeRepository 的实现
   */
  String DEFAULT_PARAMETER_NAME_DEVICEID = "deviceId";
  /**
   * 请求验证码接口时候可选参数，用于动态设置验证码超时参数
   */
  String DEFAULT_PARAMETER_NAME_EXPIRE_IN = "expireIn";
}

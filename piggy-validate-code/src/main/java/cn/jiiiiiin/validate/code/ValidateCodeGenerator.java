package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.entity.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author jiiiiiin
 */
public interface ValidateCodeGenerator {

  /**
   * 生成图形验证码
   *
   * @param request {@link ServletWebRequest}
   * @return {@link ValidateCode}
   */
  ValidateCode generate(ServletWebRequest request);
}

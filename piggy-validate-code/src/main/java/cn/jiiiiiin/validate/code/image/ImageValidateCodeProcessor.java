/**
 *
 */
package cn.jiiiiiin.validate.code.image;

import cn.jiiiiiin.validate.code.impl.AbstractValidateCodeProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 图片验证码处理器
 *
 * @author zhailiang
 */
@Component
public class ImageValidateCodeProcessor extends AbstractValidateCodeProcessor<ImageCode> {

  /**
   * 发送图形验证码，将其写到响应中
   */
  @Override
  protected void send(ServletWebRequest request, ImageCode imageCode) throws Exception {
    // 输出图片流
    imageCode.getCaptcha().out(request.getResponse().getOutputStream());
  }

}

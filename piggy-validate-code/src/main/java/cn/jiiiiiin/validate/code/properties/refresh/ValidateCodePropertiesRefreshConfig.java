package cn.jiiiiiin.validate.code.properties.refresh;

import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfigChangeListener;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.cloud.context.environment.EnvironmentChangeEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author jiiiiiin
 */
@Component
@Slf4j
@AllArgsConstructor
public class ValidateCodePropertiesRefreshConfig implements ApplicationContextAware {

    private static final String PREFIX = "jiiiiiin.security.validate";
    private final ValidateCodeProperties validateCodeProperties;
    private ApplicationContext applicationContext;

    // private final RefreshScope refreshScope;

    @ApolloConfigChangeListener(interestedKeyPrefixes = PREFIX)
    public void onChange(ConfigChangeEvent changeEvent) {
        log.debug("监听到 {} 更新通知 before refresh {}", PREFIX, validateCodeProperties
            .toString());
        // refreshScope.refresh("validateCodeProperties");

        this.applicationContext.publishEvent(new EnvironmentChangeEvent(changeEvent.changedKeys()));

        log.debug("监听到 {} 更新通知 after refresh {}", PREFIX, validateCodeProperties
            .toString());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }
}

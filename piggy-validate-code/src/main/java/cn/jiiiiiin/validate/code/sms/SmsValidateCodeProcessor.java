/**
 *
 */
package cn.jiiiiiin.validate.code.sms;

import static cn.jiiiiiin.validate.code.dict.ValidateCodeDict.DEFAULT_PARAMETER_NAME_MOBILE;

import cn.jiiiiiin.mvc.common.utils.RequestUtil;
import cn.jiiiiiin.validate.code.entity.ValidateCode;
import cn.jiiiiiin.validate.code.impl.AbstractValidateCodeProcessor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 短信验证码处理器
 *
 * @author zhailiang
 *
 */
@Component
@Slf4j
@AllArgsConstructor
public class SmsValidateCodeProcessor extends AbstractValidateCodeProcessor<ValidateCode> {

  /**
   * 短信验证码发送器
   */
  private final SmsCodeSender smsCodeSender;

  @Override
  protected void send(ServletWebRequest request, ValidateCode validateCode) throws Exception {
    String mobilePhone;
    mobilePhone = RequestUtil.getData(DEFAULT_PARAMETER_NAME_MOBILE, "短信验证码");
    log.debug("向 {} 发送短信验证码 {}", mobilePhone, validateCode.getCode());
    smsCodeSender.send(mobilePhone, validateCode.getCode());
  }

}

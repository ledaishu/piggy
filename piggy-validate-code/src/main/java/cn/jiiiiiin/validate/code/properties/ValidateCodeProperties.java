package cn.jiiiiiin.validate.code.properties;

import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 框架验证码配置类
 *
 * @author jiiiiiin
 */
@ConfigurationProperties(prefix = "jiiiiiin.security.validate")
@Setter
@Getter
@NoArgsConstructor
@RefreshScope
@ToString
@Slf4j
public class ValidateCodeProperties {

    private ImageCodeProperties imageCode = new ImageCodeProperties();
    private SmsCodeProperties smsCode = new SmsCodeProperties();

    @PostConstruct
    private void initialize() {
        log.debug("验证码相关初始化完成 initialized - imageCode: {}, smsCode: {}", imageCode, smsCode);
    }
}

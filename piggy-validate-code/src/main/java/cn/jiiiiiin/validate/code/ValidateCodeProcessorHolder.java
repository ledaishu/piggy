/**
 *
 */
package cn.jiiiiiin.validate.code;

import cn.jiiiiiin.validate.code.impl.AbstractValidateCodeProcessor;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 校验码处理器管理器
 *
 * @author zhailiang
 *
 */
@Component
@AllArgsConstructor
public class ValidateCodeProcessorHolder {

  private final Map<String, ValidateCodeProcessor> validateCodeProcessors;

  /**
   * @param type {@link ValidateCodeType}
   * @return {@link ValidateCodeProcessor}
   */
  public ValidateCodeProcessor findValidateCodeProcessor(ValidateCodeType type) {
    return findValidateCodeProcessor(type.toString().toLowerCase());
  }

  /**
   * @param type {@link ValidateCodeType}
   * @return {@link ValidateCodeProcessor}
   */
  public ValidateCodeProcessor findValidateCodeProcessor(String type) {
    String name = type.toLowerCase()
        + AbstractValidateCodeProcessor.VALIDATECODEPROCESSOR_CLASSNAME_SEPARATOR;
    ValidateCodeProcessor processor = validateCodeProcessors.get(name);
    if (processor == null) {
      throw new ValidateCodeException("验证码处理器" + name + "不存在");
    }
    return processor;
  }

}

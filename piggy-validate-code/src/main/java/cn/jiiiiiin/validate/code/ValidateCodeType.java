/**
 *
 */
package cn.jiiiiiin.validate.code;


import cn.jiiiiiin.validate.code.dict.ValidateCodeDict;

/**
 *
 * 校验码类型
 *
 * @author zhailiang
 *
 */
public enum ValidateCodeType {

  /**
   * 短信验证码
   */
  SMS {
    @Override
    public String getParamNameOnValidate() {
      return ValidateCodeDict.DEFAULT_PARAMETER_NAME_CODE_SMS;
    }
  },
  /**
   * 图片验证码
   */
  IMAGE {
    @Override
    public String getParamNameOnValidate() {
      return ValidateCodeDict.DEFAULT_PARAMETER_NAME_CODE_IMAGE;
    }
  };

  /**
   * @return 校验时从请求中获取的参数的名字
   */
  public abstract String getParamNameOnValidate();

}

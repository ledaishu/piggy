package cn.jiiiiiin.validate.code.properties;

import com.wf.captcha.base.Captcha;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 图形验证码配置类 ![](https://ws1.sinaimg.cn/large/0069RVTdgy1fuo9h1z6mrj30t30eu0tl.jpg)
 *
 * @author jiiiiiin
 */
@Setter
@Getter
@ToString(callSuper = true)
public class ImageCodeProperties extends SmsCodeProperties {

  public ImageCodeProperties() {
    super();
    // 默认2位图形验证码长度，因为默认的类型是计算类型
    setLength("2");
  }

  public enum ImageType {
    SpecCaptcha,
    GifCaptcha,
    ChineseGifCaptcha,
    ChineseCaptcha,
    ArithmeticCaptcha
  }

  /**
   * 验证码类型
   *
   * https://nicedoc.io/whvcse/EasyCaptcha?theme=light#51%E9%AA%8C%E8%AF%81%E7%A0%81%E7%B1%BB%E5%9E%8B
   */
  private ImageType codeImgType = ImageType.ArithmeticCaptcha;

  /**
   * 验证码字符类型
   *
   * https://nicedoc.io/whvcse/EasyCaptcha?theme=light#52%E9%AA%8C%E8%AF%81%E7%A0%81%E5%AD%97%E7%AC%A6%E7%B1%BB%E5%9E%8B
   */
  private int codeTextType = Captcha.TYPE_DEFAULT;

  /**
   * 图形验证码宽度
   */
  private String width = "100";
  /**
   * 图形验证码高度
   */
  private String height = "40";
  /**
   * 验证码文本字符大小  默认为30
   */
  private String size = "30";

}

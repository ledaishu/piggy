package cn.jiiiiiin.validate.code.sms;

import cn.jiiiiiin.validate.code.ValidateCodeGenerator;
import cn.jiiiiiin.validate.code.dict.ValidateCodeDict;
import cn.jiiiiiin.validate.code.entity.ValidateCode;
import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 默认的图形验证码生成器
 *
 * @author jiiiiiin
 */
@Component
public class SmsValidateCodeGenerator implements ValidateCodeGenerator {

    @Autowired
    private ValidateCodeProperties validateCodeProperties;

    @Override
    public ValidateCode generate(ServletWebRequest request) {
        final int expireIn = ServletRequestUtils.getIntParameter(request.getRequest(), ValidateCodeDict.DEFAULT_PARAMETER_NAME_EXPIRE_IN, validateCodeProperties.getSmsCode().getExpireIn());
        // 生成验证码
        final String code = RandomStringUtils.randomNumeric(Integer.parseInt(validateCodeProperties.getSmsCode().getLength()));
        final ValidateCode validateCode = new ValidateCode(code, expireIn);
        return validateCode;
    }
}

package cn.jiiiiiin.validate.code.sms;

import lombok.extern.slf4j.Slf4j;

/**
 * 默认实现
 *
 * @author jiiiiiin
 */
@Slf4j
public class DefaultSmsCodeSender implements SmsCodeSender {

  @Override
  public void send(String mobilePhone, String validateCode) {
    log.error("请配置真实的验证码发送器");
  }

}

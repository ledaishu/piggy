package cn.jiiiiiin.validate.code.sms;


import cn.jiiiiiin.validate.code.ValidateCodeCheckFailureHandler;
import cn.jiiiiiin.validate.code.ValidateCodeException;
import cn.jiiiiiin.validate.code.ValidateCodeProcessorHolder;
import cn.jiiiiiin.validate.code.ValidateCodeType;
import cn.jiiiiiin.validate.code.properties.ValidateCodeProperties;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * 验证码校验
 * <p>
 * 包括图形、短信验证码的校验逻辑
 * <p>
 * 被配置到ss框架过滤器链的`org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter`之前执行
 *
 * @author jiiiiiin
 */
@Component
@Slf4j
public class ValidateCodeFilter extends OncePerRequestFilter {

  /**
   * 系统配置信息
   */
  private final ValidateCodeProperties validateCodeProperties;

  /**
   * 系统中的校验码处理器
   */
  private final ValidateCodeProcessorHolder validateCodeProcessorHolder;

  /**
   * 验证请求url与配置的url是否匹配的工具类
   */
  private AntPathMatcher pathMatcher = new AntPathMatcher();

  private final ValidateCodeCheckFailureHandler validateCodeCheckFailureHandler;

  public ValidateCodeFilter(ValidateCodeProperties validateCodeProperties,
      ValidateCodeCheckFailureHandler validateCodeCheckFailureHandler,
      ValidateCodeProcessorHolder validateCodeProcessorHolder) {
    this.validateCodeProperties = validateCodeProperties;
    this.validateCodeCheckFailureHandler = validateCodeCheckFailureHandler;
    this.validateCodeProcessorHolder = validateCodeProcessorHolder;
  }

  /**
   * 进行验证码的校验
   * <p>
   * 通用校验器
   * <p>
   * 如：拦截身份认证表单提交请求等配置在当前 验证码对象中的`ValidateCodeFilter`配置的请求
   * <p>
   * {@inheritDoc}
   */
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    final ValidateCodeType type = getValidateCodeType(request, validateCodeProperties, pathMatcher);
    if (type != null) {
      logger.debug("校验请求(" + request.getRequestURI() + ")中的验证码,验证码类型" + type);
      try {
        validateCodeProcessorHolder.findValidateCodeProcessor(type)
            .validate(new ServletWebRequest(request, response));
        logger.debug("验证码校验通过");
      } catch (ValidateCodeException exception) {
        logger.error("验证码校验失败", exception);
        // 统一身份认证异常处理
        validateCodeCheckFailureHandler.onValidateFailure(request, response, exception);
        return;
      }
    }
    filterChain.doFilter(request, response);
  }

  /**
   * 获取校验码的类型，如果当前请求不需要校验，则返回null
   * <p>
   * {@inheritDoc}
   *
   * @return 返回null则标识当前请求不在需要进行验证码校验的配置范畴
   */
  public static ValidateCodeType getValidateCodeType(HttpServletRequest request,
      ValidateCodeProperties validateCodeProperties, AntPathMatcher pathMatcher) {
    if (!StringUtils.equalsIgnoreCase(request.getMethod(), "GET")) {

      val imageCode = validateCodeProperties.getImageCode();
      for (String imgCodeInterceptorUrl : imageCode.getInterceptorUrls()) {
        if (pathMatcher.match(imgCodeInterceptorUrl, request.getRequestURI())) {
          return ValidateCodeType.IMAGE;
        }
      }

      val smsCode = validateCodeProperties.getSmsCode();
      for (String smsCodeInterceptorUrl : smsCode.getInterceptorUrls()) {
        if (pathMatcher.match(smsCodeInterceptorUrl, request.getRequestURI())) {
          return ValidateCodeType.SMS;
        }
      }

    }
    return null;
  }
}

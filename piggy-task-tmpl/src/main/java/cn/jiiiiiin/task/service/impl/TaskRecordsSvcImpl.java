package cn.jiiiiiin.task.service.impl;

import cn.jiiiiiin.task.entity.TaskRecords;
import cn.jiiiiiin.task.mapper.TaskRecordsMapper;
import cn.jiiiiiin.task.service.ITaskRecordsSvc;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service("taskRecordsSvc")
@AllArgsConstructor
public class TaskRecordsSvcImpl extends
    ServiceImpl<TaskRecordsMapper, TaskRecords> implements
    ITaskRecordsSvc {

}

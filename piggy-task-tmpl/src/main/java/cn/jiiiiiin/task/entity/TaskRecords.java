package cn.jiiiiiin.task.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;

/**
 * @author zhaojun
 */
@NoArgsConstructor
@TableName("task_records")
@Data
@Accessors(chain = true)
public class TaskRecords {

  @Getter
  public enum FieldDict {

    STATUS_PROCESSING("9", "处理中"),
    STATUS_FAIL("1", "成功"),
    STATUS_OK("0", "失败");;

    private final String name;
    private final String value;

    /**
     * @param value 字段对于字典值
     * @param name  字段字典描述
     */
    FieldDict(@NonNull String value, @NonNull String name) {
      this.name = name;
      this.value = value;
    }
  }

  @JsonProperty(value = "id")
  private String id;

  /**
   * 任务唯一标示
   */
  private String taskId;

  /**
   * 处理日期，清算执行时间
   */
  private Date processDate;

  /**
   * 报送状态：0- 处理成功、1- 处理出错、9- 处理中
   */
  private String status;

  /**
   * 执行次数
   */
  private int numbOfExecutions;

  /**
   * 当前记录执行的节点IP
   */
  private String nodeIp;
}

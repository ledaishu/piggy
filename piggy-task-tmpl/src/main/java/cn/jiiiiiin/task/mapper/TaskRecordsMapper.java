package cn.jiiiiiin.task.mapper;

import cn.jiiiiiin.task.entity.TaskRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import lombok.NonNull;
import org.apache.ibatis.annotations.Param;

public interface TaskRecordsMapper extends BaseMapper<TaskRecords> {

  /**
   * @param taskId       待查询的任务标示ID
   * @param intervalFlag 间隔标示 M标示按月、D标示按天，来查询{@link TaskRecords#processDate}对于时间区间内的数据
   * @return 获取对应任务执行的最后次数
   */
  Integer getLastTaskNumbOfExecution(@NonNull @Param("taskId") String taskId,
      @Param("intervalFlag") @NonNull String intervalFlag);
}

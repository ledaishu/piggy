package cn.jiiiiiin.task.core;

import cn.hutool.core.util.EnumUtil;
import cn.jiiiiiin.mvc.common.utils.IPUtil;
import cn.jiiiiiin.task.core.ITask.IntervalFlag;
import cn.jiiiiiin.task.core.TaskException.ERR;
import cn.jiiiiiin.task.entity.TaskRecords;
import cn.jiiiiiin.task.entity.TaskRecords.FieldDict;
import cn.jiiiiiin.task.mapper.TaskRecordsMapper;
import cn.jiiiiiin.task.service.ITaskRecordsSvc;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import java.util.Date;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;

/**
 * 任务执行模版，帮助应用检查和记录当前任务周期内，计划任务的执行情况
 */
@Slf4j
@Component
@AllArgsConstructor
public class TaskTmpl {

  private final ITaskRecordsSvc taskRecordsSvc;

  private final TaskRecordsMapper taskRecordsMapper;

  public void execute(@NonNull ITask task) {
    val taskId = task.getId();
    log.info("=== taskTmpl execute 准备执行任务 {} ===", taskId);
    // 第一个判断是确保当前任务如果已经成功执行过就不会再被重复执行
    if (checkCurrStatus(task)) {
      log.info("当天已经执行成功过 {} taskTmpl 完结", task.getId());
      return;
    }
    // 第二个判断是给具体的任务去自行判断是否还需要执行
    if (task.isNeedRunTask()) {
      log.info("{} 任务准备开始执行", taskId);
      val currRecords = _insertReportRecords(task);
      log.info("{} 任务记录登记为执行中 {}", taskId, currRecords);
      boolean processStatus = false;
      try {
        processStatus = task.runTask(currRecords);
        log.info("{} 任务内部流程支持成功", taskId);
      } catch (Exception e) {
        log.error("任务内部流程执行出现异常", e);
      } finally {
        _updateCurrRecordsStatus(processStatus, currRecords);
        log.info("{} 任务更新记录登记 {}", taskId, currRecords);
        log.info("=== taskTmpl execute 任务{}执行完毕 ===", taskId);
      }
    }
  }

  private boolean checkCurrStatus(@NonNull ITask task) {
    if (task.needCheckIntervalTimeTaskStatus()) {
      log.info("检查当天是否已经执行成功过 {}", task.getId());
      return intervalTimeIsProcessed(task);
    }
    return false;
  }

  /**
   * 检查间隔时间内是否有成功记录
   * @param task {@link ITask}
   * @return 当前周期是否执行成功过计划任务
   */
  private boolean intervalTimeIsProcessed(@NonNull ITask task) {
    val INTERVAL_FLAG = task.getIntervalFlag();
    val contains = EnumUtil.contains(ITask.IntervalFlag.class, INTERVAL_FLAG.name());
    if (!contains) {
      throw new TaskException(ERR.NO_SUPPORT_PS_PR);
    }
    val queryWrapper = new LambdaQueryWrapper<TaskRecords>()
        .eq(TaskRecords::getTaskId, task.getId())
        .eq(TaskRecords::getStatus, FieldDict.STATUS_OK.getValue())
        .apply(INTERVAL_FLAG.equals(IntervalFlag.D), "to_days(process_date) = to_days(now())")
        .apply(INTERVAL_FLAG.equals(IntervalFlag.M),
            "date_format(process_date, '%Y%m') = date_format(curdate() , '%Y%m')");
    val currOKRecord = taskRecordsSvc.getOne(queryWrapper);
    val isProcessOk = !Objects.isNull(currOKRecord);
    if (isProcessOk) {
      log.info("当前任务 {} 已经于{}，执行到第{}次，成功执行", task.getId(), currOKRecord.getProcessDate(),
          currOKRecord.getNumbOfExecutions());
    }
    return isProcessOk;
  }

  private void _updateCurrRecords2Fail(TaskRecords currRecords) {
    currRecords.setStatus(FieldDict.STATUS_FAIL.getValue());
    taskRecordsSvc.updateById(currRecords);
  }

  private void _updateCurrRecords2OK(TaskRecords currRecords) {
    currRecords.setStatus(FieldDict.STATUS_OK.getValue());
    taskRecordsSvc.updateById(currRecords);
  }

  private TaskRecords _insertReportRecords(@NonNull ITask task) {
    val taskId = task.getId();
    val lastTaskNumbOfExecution = taskRecordsMapper
        .getLastTaskNumbOfExecution(taskId, task.getIntervalFlag().name());
    int numbOfExecutions = 1;
    if (!Objects.isNull(lastTaskNumbOfExecution)) {
      numbOfExecutions += lastTaskNumbOfExecution;
    }
    val currRecord = new TaskRecords()
        .setTaskId(taskId)
        .setStatus(FieldDict.STATUS_PROCESSING.getValue())
        .setProcessDate(new Date())
        .setNodeIp(IPUtil.getLocalHostExactAddress().getHostAddress())
        .setNumbOfExecutions(numbOfExecutions);
    taskRecordsSvc.save(currRecord);
    return currRecord;
  }

  /**
   * 回滚上报锁状态
   *
   * @param processStatus 状态
   * @param currRecords   原记录
   */
  private void _updateCurrRecordsStatus(boolean processStatus,
      TaskRecords currRecords) {
    log.info("准备更新任务记录状态 {} {}", processStatus, currRecords);
    if (processStatus) {
      _updateCurrRecords2OK(currRecords);
    } else {
      _updateCurrRecords2Fail(currRecords);
    }
  }

}

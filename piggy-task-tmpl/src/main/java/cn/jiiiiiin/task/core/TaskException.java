package cn.jiiiiiin.task.core;

import lombok.Getter;
import lombok.NonNull;

public class TaskException extends RuntimeException {

  /**
   * 错误编码
   */
  private int code;

  /**
   * 返回到前端错误消息后缀,标示为对方方发生的错误
   */
  public static final String ERR_PREFIX = " - t";

  @Getter
  public enum ERR {
    NO_SUPPORT_PS_PR(4001, "请提交PR以支持其他类型判断"),
    CURR_TASK_IS_CURR_DATE_PROCESSED(4000, "当前任务当天已经处理完成"),
    ;
    private int code;
    private String message;

    ERR(int code, String message) {
      this.code = code;
      this.message = message;
    }

  }

  public TaskException(@NonNull ERR err) {
    super(err.getMessage());
    this.code = err.code;
  }

  public TaskException(@NonNull ERR err, Exception e) {
    super(err.getMessage(), e);
    this.code = err.code;
  }
}

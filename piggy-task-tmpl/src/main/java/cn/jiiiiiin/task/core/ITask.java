package cn.jiiiiiin.task.core;

import cn.jiiiiiin.task.entity.TaskRecords;
import lombok.NonNull;

/**
 * 定时任务模版
 *
 * @author zhaojun
 */
public interface ITask {

  /**
   * 需要为每个任务分配一个唯一ID
   *
   * @return 任务ID
   */
  String getId();

  /**
   * 默认按当天查询{@link ITask#needCheckIntervalTimeTaskStatus()} 任务执行状态
   *
   * @return 默认按当天统计是否在执行成功过一次
   */
  default IntervalFlag getIntervalFlag() {
    return IntervalFlag.D;
  }

  /**
   * 是否判断当前任务处理日（当天是否）已经执行成功过
   *
   * @return default true 标示需要判断{@link ITask#getIntervalFlag()} 期间当前任务是否执行成功过
   */
  default boolean needCheckIntervalTimeTaskStatus() {
    return true;
  }

  /**
   * 判断本次任务是否还需往下继续执行，比如有些任务只要执行成功一次当天就无需在执行的情况，可以实现改接口，返回false则本次任务就被中断
   *
   * @return true 需要往下执行
   */
  default boolean isNeedRunTask() {
    return true;
  }

  /**
   * 执行任务
   *
   * @param currRecords 当前执行任务的记录对象，相关信息{@link TaskRecords}
   * @return 任务是否执行成功，如抛出异常则标示任务执行失败，等同返回false
   */
  boolean runTask(@NonNull TaskRecords currRecords);


  public enum IntervalFlag {
    /**
     * 标示按当月统计是否在执行成功过一次
     */
    M,
    /**
     * 标示按当天统计是否在执行成功过一次
     */
    D
  }
}

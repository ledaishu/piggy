DROP TABLE IF EXISTS shedlock;
CREATE TABLE shedlock
(
    name       VARCHAR(64)  NOT NULL,
    lock_until TIMESTAMP(3) NOT NULL,
    locked_at  TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    locked_by  VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
) COMMENT '分布式锁控制表';

DROP TABLE IF EXISTS `task_records`;
CREATE TABLE `task_records`
(
    `id`                 bigint(20)  NOT NULL COMMENT '记录id',
    `task_id`            varchar(16) NOT NULL COMMENT '任务唯一表示',
    `process_date`       datetime    NOT NULL COMMENT '处理日期，清算执行时间',
    `status`             char(1)     NOT NULL COMMENT '报送状态：0- 处理成功、1- 处理出错、9- 处理中',
    `numb_of_executions` integer unsigned NOT NULL DEFAULT 1 COMMENT '当前执行区间内，任务的执行次数',
    `node_ip`            varchar(16) NOT NULL COMMENT '当前记录执行的节点IP',
    PRIMARY KEY (`id`)
) COMMENT '定时任务执行记录表';

CREATE INDEX task_records_process_date_index
    ON task_records (process_date);
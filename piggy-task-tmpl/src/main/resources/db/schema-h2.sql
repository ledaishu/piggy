-- 避免mysql对应函数h2不支持
CREATE ALIAS TO_DAYS FOR "org.mvnsearch.h2.mysql.DateTimeFunctions.toDays";
DROP TABLE IF EXISTS `task_records`;
CREATE TABLE `task_records`
(
    `id`                 BIGINT(20)  NOT NULL COMMENT '记录id',
    `task_id`            varchar(16) NOT NULL COMMENT '任务唯一表示',
    `process_date`       datetime    NOT NULL COMMENT '处理日期，清算执行时间',
    `status`             char(1)     NOT NULL COMMENT '报送状态：0- 处理成功、1- 处理出错、9- 处理中',
    `numb_of_executions` integer NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE INDEX task_records_process_date_index
    ON task_records (process_date);

DROP TABLE IF EXISTS shedlock;

--  MySQL, MariaDB
CREATE TABLE shedlock
(
    name       VARCHAR(64)  NOT NULL,
    lock_until TIMESTAMP(3) NOT NULL,
    locked_at  TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    locked_by  VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
);



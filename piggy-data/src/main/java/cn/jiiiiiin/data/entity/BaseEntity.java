package cn.jiiiiiin.data.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 基础父类测试
 * </p>
 *
 * @author hubin
 * @author jiiiiiin
 * @since 2018-08-11
 */
@Getter
@Setter
@Accessors(chain = true)
@Slf4j
public class BaseEntity<T extends Model<?>> extends Model<T> {

    public interface IDGroup {
    }

    private static final long serialVersionUID = 7792309132836966596L;

    /**
     * 数据库表主键
     * <p>
     * http://mp.baomidou.com/guide/faq.html#id-worker-生成主键太长导致-js-精度丢失
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "参数id不能为空", groups = {IDGroup.class})
    private Long id;

    public static final String ID = "id";

    @Override
    protected Serializable pkVal() {
        /**
         * AR
         *
         *
         * 模式这个必须有，否则 xxById 的方法都将失效！
         * 另外 UserMapper 也必须 AR 依赖该层注入，有可无 XML
         */
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseEntity)) {
            return false;
        }
        BaseEntity<?> that = (BaseEntity<?>) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

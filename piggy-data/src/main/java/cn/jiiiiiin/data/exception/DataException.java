package cn.jiiiiiin.data.exception;

import lombok.Getter;
import lombok.NonNull;

/**
 * 应用业务异常
 *
 * @author zhaojun
 */
@Getter
public class DataException extends RuntimeException {

  /**
   * 错误编码
   */
  private int code;

  /**
   * 返回到前端错误消息后缀,标示为对方方发生的错误
   */
  public static final String ERR_PREFIX = " - jd";

  @Getter
  public enum ERR {
    FN2FIELD_NAME_ERR(9000, "获取所需字段名错误");
    private final int code;
    private final String message;

    ERR(@NonNull int code, @NonNull String message) {
      this.code = code;
      this.message = message.concat(ERR_PREFIX);
    }
  }

  public DataException(@NonNull DataException.ERR err) {
    super(err.getMessage());
    this.code = err.code;
  }

  public DataException(@NonNull DataException.ERR err, @NonNull Exception e) {
    super(err.getMessage(), e);
    this.code = err.code;
  }
}

package cn.jiiiiiin.data.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author jiiiiiin
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {

  // /**
  //  * 分页插件
  //  */
  // @Bean
  // public PaginationInterceptor paginationInterceptor() {
  //   return new PaginationInterceptor();
  // }
  //
  // @Bean
  // public SqlExplainInterceptor sqlExplainInterceptor() {
  //   //启用执行分析插件
  //   return new SqlExplainInterceptor();
  // }


}

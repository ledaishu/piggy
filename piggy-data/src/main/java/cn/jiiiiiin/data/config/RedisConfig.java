package cn.jiiiiiin.data.config;

import lombok.extern.slf4j.Slf4j;
// import org.springframework.boot.autoconfigure.AutoConfigureAfter;
// import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
// import org.springframework.data.redis.core.RedisTemplate;
// import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
// import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 其中@Configuration 代表这个类是一个配置类，然后@AutoConfigureAfter(RedisAutoConfiguration.class)
 * 是让我们这个配置类在内置的配置类之后在配置， 这样就保证我们的配置类生效，并且不会被覆盖配置。其中需要注意的就是方法名一定要叫redisTemplate
 * 因为@Bean注解是根据方法名配置这个bean的name的。
 * <p>
 * https://www.jianshu.com/p/feef1421ab0b
 *
 * http://www.iocoder.cn/Spring-Boot/battcn/v2-nosql-redis/
 *
 * TODO 解决如果应用不需要使用redis的情况
 *
 * @author jiiiiiin
 */
// @Configuration
// @AutoConfigureAfter(RedisAutoConfiguration.class)
@Slf4j
public class RedisConfig {

  // @Bean
  // public RedisTemplate<String, Object> redisTemplate(
  //     LettuceConnectionFactory redisConnectionFactory) {
  //   RedisTemplate<String, Object> template = new RedisTemplate<>();
  //   template.setKeySerializer(new StringRedisSerializer());
  //   template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
  //   template.setConnectionFactory(redisConnectionFactory);
  //   return template;
  // }

}

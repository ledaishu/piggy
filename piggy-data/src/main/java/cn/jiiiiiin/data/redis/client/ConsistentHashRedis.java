package cn.jiiiiiin.data.redis.client;

import lombok.NonNull;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
// import redis.clients.jedis.*;
// import redis.clients.jedis.commands.JedisCommands;
// import redis.clients.jedis.params.GeoRadiusParam;
// import redis.clients.jedis.params.SetParams;
// import redis.clients.jedis.params.ZAddParams;
// import redis.clients.jedis.params.ZIncrByParams;
//
// import java.io.Closeable;
// import java.io.IOException;
// import java.util.List;
// import java.util.Map;
// import java.util.Set;

/**
 * 一致性哈希Redis
 *
 * @author zetee
 */
public class ConsistentHashRedis {
    // public class ConsistentHashRedis  implements JedisCommands {
    // private  ShardedJedisPool pool;
    // private GenericObjectPoolConfig poolConfig;
    // private  List<JedisShardInfo> shards;
    //
    //
    // public  ConsistentHashRedis(@NonNull GenericObjectPoolConfig poolConfig,@NonNull  List<JedisShardInfo> shards){
    //     this.poolConfig=poolConfig;
    //     this.shards=shards;
    //     pool=new ShardedJedisPool(poolConfig,shards);
    // }
    //
    //
    // @Override
    // public String set(String key, String value) {
    //     try(ShardedJedis j= pool.getResource()){
    //         return j.set(key,value);
    //     }
    // }
    //
    // @Override
    // public String set(String key, String value, SetParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.set(key, value, params);
    //     }
    // }
    //
    // @Override
    // public String get(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.get(key);
    //     }
    // }
    //
    // @Override
    // public Boolean exists(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.exists(key);
    //     }
    // }
    //
    // @Override
    // public Long persist(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.persist(key);
    //     }
    // }
    //
    // @Override
    // public String type(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.type(key);
    //     }
    // }
    //
    // @Override
    // public byte[] dump(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.dump(key);
    //     }
    // }
    //
    // @Override
    // public String restore(String key, int ttl, byte[] serializedValue) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.restore(key, ttl, serializedValue);
    //     }
    // }
    //
    // @Override
    // public String restoreReplace(String key, int ttl, byte[] serializedValue) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.restoreReplace(  key,   ttl,  serializedValue);
    //     }
    // }
    //
    // @Override
    // public Long expire(String key, int seconds) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. expire(  key,   seconds);
    //     }
    // }
    //
    // @Override
    // public Long pexpire(String key, long milliseconds) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.pexpire(  key,   milliseconds);
    //     }
    // }
    //
    // @Override
    // public Long expireAt(String key, long unixTime) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.expireAt(  key,   unixTime);
    //     }
    // }
    //
    // @Override
    // public Long pexpireAt(String key, long millisecondsTimestamp) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.pexpireAt(  key,   millisecondsTimestamp);
    //     }
    // }
    //
    // @Override
    // public Long ttl(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.ttl(key);
    //     }
    // }
    //
    // @Override
    // public Long pttl(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.pttl(  key);
    //     }
    // }
    //
    // @Override
    // public Long touch(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.touch(  key);
    //     }
    // }
    //
    // @Override
    // public Boolean setbit(String key, long offset, boolean value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.setbit(  key,   offset,   value);
    //     }
    // }
    //
    // @Override
    // public Boolean setbit(String key, long offset, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.setbit(  key,   offset,   value);
    //     }
    // }
    //
    // @Override
    // public Boolean getbit(String key, long offset) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.getbit(  key,   offset);
    //     }
    // }
    //
    // @Override
    // public Long setrange(String key, long offset, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.setrange(  key,   offset,   value);
    //     }
    // }
    //
    // @Override
    // public String getrange(String key, long startOffset, long endOffset) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.getrange(  key,   startOffset,   endOffset);
    //     }
    // }
    //
    // @Override
    // public String getSet(String key, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. getSet(  key,   value);
    //     }
    // }
    //
    // @Override
    // public Long setnx(String key, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.setnx(  key,   value) ;
    //     }
    // }
    //
    // @Override
    // public String setex(String key, int seconds, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.setex(  key,   seconds,   value);
    //     }
    // }
    //
    // @Override
    // public String psetex(String key, long milliseconds, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.psetex(  key,   milliseconds,   value);
    //     }
    // }
    //
    // @Override
    // public Long decrBy(String key, long decrement) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.decrBy(  key,   decrement);
    //     }
    // }
    //
    // @Override
    // public Long decr(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.decr(  key);
    //     }
    // }
    //
    // @Override
    // public Long incrBy(String key, long increment) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.incrBy(  key,   increment);
    //     }
    // }
    //
    // @Override
    // public Double incrByFloat(String key, double increment) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.incrByFloat(  key,   increment);
    //     }
    // }
    //
    // @Override
    // public Long incr(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.incr(  key);
    //     }
    // }
    //
    // @Override
    // public Long append(String key, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.append(  key,   value);
    //     }
    // }
    //
    // @Override
    // public String substr(String key, int start, int end) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.substr(  key,   start,   end);
    //     }
    // }
    //
    // @Override
    // public Long hset(String key, String field, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hset(  key,   field,   value);
    //     }
    // }
    //
    // @Override
    // public Long hset(String key, Map<String, String> hash) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hset(  key,   hash);
    //     }
    // }
    //
    // @Override
    // public String hget(String key, String field) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hget(  key,   field);
    //     }
    // }
    //
    // @Override
    // public Long hsetnx(String key, String field, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hsetnx(  key,   field,   value);
    //     }
    // }
    //
    // @Override
    // public String hmset(String key, Map<String, String> hash) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hmset(  key,   hash);
    //     }
    // }
    //
    // @Override
    // public List<String> hmget(String key, String... fields) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hmget(  key,   fields);
    //     }
    // }
    //
    // @Override
    // public Long hincrBy(String key, String field, long value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hincrBy(  key,   field,   value);
    //     }
    // }
    //
    // @Override
    // public Double hincrByFloat(String key, String field, double value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hincrByFloat(  key,   field,   value);
    //     }
    // }
    //
    // @Override
    // public Boolean hexists(String key, String field) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hexists(  key,   field);
    //     }
    // }
    //
    // @Override
    // public Long hdel(String key, String... field) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hdel(  key,   field);
    //     }
    // }
    //
    // @Override
    // public Long hlen(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hlen(  key);
    //     }
    // }
    //
    // @Override
    // public Set<String> hkeys(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hkeys(  key);
    //     }
    // }
    //
    // @Override
    // public List<String> hvals(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hvals(  key);
    //     }
    // }
    //
    // @Override
    // public Map<String, String> hgetAll(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. hgetAll(  key) ;
    //     }
    // }
    //
    // @Override
    // public Long rpush(String key, String... string) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. rpush(  key,   string);
    //     }
    // }
    //
    // @Override
    // public Long lpush(String key, String... string) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lpush(  key,  string);
    //     }
    // }
    //
    // @Override
    // public Long llen(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.llen(  key);
    //     }
    // }
    //
    // @Override
    // public List<String> lrange(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lrange(  key,   start,   stop) ;
    //     }
    // }
    //
    // @Override
    // public String ltrim(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.ltrim(  key,   start,   stop);
    //     }
    // }
    //
    // @Override
    // public String lindex(String key, long index) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lindex(  key,   index);
    //     }
    // }
    //
    // @Override
    // public String lset(String key, long index, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lset(  key,   index,   value);
    //     }
    // }
    //
    // @Override
    // public Long lrem(String key, long count, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lrem(  key,   count,   value);
    //     }
    // }
    //
    // @Override
    // public String lpop(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lpop(  key);
    //     }
    // }
    //
    // @Override
    // public String rpop(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.rpop(  key);
    //     }
    // }
    //
    // @Override
    // public Long sadd(String key, String... member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sadd(  key,    member);
    //     }
    // }
    //
    // @Override
    // public Set<String> smembers(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.smembers(  key);
    //     }
    // }
    //
    // @Override
    // public Long srem(String key, String... member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.srem(  key,   member);
    //     }
    // }
    //
    // @Override
    // public String spop(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.spop(  key) ;
    //     }
    // }
    //
    // @Override
    // public Set<String> spop(String key, long count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.spop(  key,   count);
    //     }
    // }
    //
    // @Override
    // public Long scard(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.scard(  key);
    //     }
    // }
    //
    // @Override
    // public Boolean sismember(String key, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sismember(  key,   member);
    //     }
    // }
    //
    // @Override
    // public String srandmember(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.srandmember(  key);
    //     }
    // }
    //
    // @Override
    // public List<String> srandmember(String key, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.srandmember(  key,   count);
    //     }
    // }
    //
    // @Override
    // public Long strlen(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.strlen(  key);
    //     }
    // }
    //
    // @Override
    // public Long zadd(String key, double score, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zadd(  key,   score,   member) ;
    //     }
    // }
    //
    // @Override
    // public Long zadd(String key, double score, String member, ZAddParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zadd(  key,   score,   member,   params);
    //     }
    // }
    //
    // @Override
    // public Long zadd(String key, Map<String, Double> scoreMembers) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zadd( key,  scoreMembers) ;
    //     }
    // }
    //
    // @Override
    // public Long zadd(String key, Map<String, Double> scoreMembers, ZAddParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zadd( key,  scoreMembers,   params);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrange(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrange( key,  start,  stop);
    //     }
    // }
    //
    // @Override
    // public Long zrem(String key, String... members) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrem( key,  members);
    //     }
    // }
    //
    // @Override
    // public Double zincrby(String key, double increment, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zincrby( key,  increment,  member);
    //     }
    // }
    //
    // @Override
    // public Double zincrby(String key, double increment, String member, ZIncrByParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zincrby( key,  increment,  member,   params) ;
    //     }
    // }
    //
    // @Override
    // public Long zrank(String key, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrank( key,  member);
    //     }
    // }
    //
    // @Override
    // public Long zrevrank(String key, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrank( key,  member) ;
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrange(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrange( key,  start,  stop);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrangeWithScores(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeWithScores( key,  start,  stop);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrevrangeWithScores(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeWithScores( key,  start,  stop);
    //     }
    // }
    //
    // @Override
    // public Long zcard(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zcard(  key) ;
    //     }
    // }
    //
    // @Override
    // public Double zscore(String key, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zscore( key,  member);
    //     }
    // }
    //
    // @Override
    // public Tuple zpopmax(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zpopmax( key);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zpopmax(String key, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. zpopmax( key,  count) ;
    //     }
    // }
    //
    // @Override
    // public Tuple zpopmin(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zpopmin( key);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zpopmin(String key, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. zpopmin( key,  count);
    //     }
    // }
    //
    // @Override
    // public List<String> sort(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sort( key);
    //     }
    // }
    //
    // @Override
    // public List<String> sort(String key, SortingParams sortingParameters) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sort( key,   sortingParameters);
    //     }
    // }
    //
    // @Override
    // public Long zcount(String key, double min, double max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zcount( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Long zcount(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zcount( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByScore(String key, double min, double max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScore( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByScore(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScore( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByScore(String key, double max, double min) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScore( key,  max,  min);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByScore(String key, double min, double max, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScore( key,  min,  max,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByScore(String key, String max, String min) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScore( key,  max,  min);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByScore(String key, String min, String max, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScore( key,  min,  max,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByScore(String key, double max, double min, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScore( key,  max,  min,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrangeByScoreWithScores(String key, double min, double max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScoreWithScores( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrevrangeByScoreWithScores(String key, double max, double min) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScoreWithScores( key,  max,  min) ;
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrangeByScoreWithScores(String key, double min, double max, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScoreWithScores( key,  min,  max,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByScore(String key, String max, String min, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScore( key,  max,  min,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrangeByScoreWithScores(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByScoreWithScores( key,  min,  max) ;
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrevrangeByScoreWithScores(String key, String max, String min) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScoreWithScores( key,  max,  min);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrangeByScoreWithScores(String key, String min, String max, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j. zrangeByScoreWithScores( key,  min,  max,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrevrangeByScoreWithScores(String key, double max, double min, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScoreWithScores( key,  max,  min,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<Tuple> zrevrangeByScoreWithScores(String key, String max, String min, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByScoreWithScores( key,  max,  min,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Long zremrangeByRank(String key, long start, long stop) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zremrangeByRank( key,  start,  stop) ;
    //     }
    // }
    //
    // @Override
    // public Long zremrangeByScore(String key, double min, double max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zremrangeByScore( key,  min,  max) ;
    //     }
    // }
    //
    // @Override
    // public Long zremrangeByScore(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zremrangeByScore( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Long zlexcount(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zlexcount( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByLex(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByLex( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrangeByLex(String key, String min, String max, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrangeByLex( key,  min,  max,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByLex(String key, String max, String min) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByLex( key,  max,  min);
    //     }
    // }
    //
    // @Override
    // public Set<String> zrevrangeByLex(String key, String max, String min, int offset, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zrevrangeByLex( key,  max,  min,  offset,  count);
    //     }
    // }
    //
    // @Override
    // public Long zremrangeByLex(String key, String min, String max) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zremrangeByLex( key,  min,  max);
    //     }
    // }
    //
    // @Override
    // public Long linsert(String key, ListPosition where, String pivot, String value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.linsert( key,   where,  pivot,  value);
    //     }
    // }
    //
    // @Override
    // public Long lpushx(String key, String... string) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.lpushx( key,string  );
    //     }
    // }
    //
    // @Override
    // public Long rpushx(String key, String... string) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.rpushx( key, string );
    //     }
    // }
    //
    // @Override
    // public List<String> blpop(int timeout, String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.blpop( timeout,  key);
    //     }
    // }
    //
    // @Override
    // public List<String> brpop(int timeout, String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.brpop( timeout,  key);
    //     }
    // }
    //
    // @Override
    // public Long del(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.del( key);
    //     }
    // }
    //
    // @Override
    // public Long unlink(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.unlink( key);
    //     }
    // }
    //
    // @Override
    // public String echo(String string) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.echo(string);
    //     }
    // }
    //
    // @Override
    // public Long move(String key, int dbIndex) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.move( key,  dbIndex);
    //     }
    // }
    //
    // @Override
    // public Long bitcount(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.bitcount( key) ;
    //     }
    // }
    //
    // @Override
    // public Long bitcount(String key, long start, long end) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.bitcount( key,  start,  end);
    //     }
    // }
    //
    // @Override
    // public Long bitpos(String key, boolean value) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.bitpos( key,  value) ;
    //     }
    // }
    //
    // @Override
    // public Long bitpos(String key, boolean value, BitPosParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.bitpos( key,  value,   params);
    //     }
    // }
    //
    // @Override
    // public ScanResult<Map.Entry<String, String>> hscan(String key, String cursor) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hscan( key,  cursor);
    //     }
    // }
    //
    // @Override
    // public ScanResult<Map.Entry<String, String>> hscan(String key, String cursor, ScanParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hscan( key,  cursor,   params) ;
    //     }
    // }
    //
    // @Override
    // public ScanResult<String> sscan(String key, String cursor) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sscan( key,  cursor);
    //     }
    // }
    //
    // @Override
    // public ScanResult<Tuple> zscan(String key, String cursor) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zscan( key,  cursor);
    //     }
    // }
    //
    // @Override
    // public ScanResult<Tuple> zscan(String key, String cursor, ScanParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.zscan( key,  cursor,   params);
    //     }
    // }
    //
    // @Override
    // public ScanResult<String> sscan(String key, String cursor, ScanParams params) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.sscan( key,  cursor,   params);
    //     }
    // }
    //
    // @Override
    // public Long pfadd(String key, String... elements) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.pfadd( key,  elements);
    //     }
    // }
    //
    // @Override
    // public long pfcount(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.pfcount(  key);
    //     }
    // }
    //
    // @Override
    // public Long geoadd(String key, double longitude, double latitude, String member) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geoadd( key,  longitude,  latitude,  member);
    //     }
    // }
    //
    // @Override
    // public Long geoadd(String key, Map<String, GeoCoordinate> memberCoordinateMap) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geoadd( key,   memberCoordinateMap) ;
    //     }
    // }
    //
    // @Override
    // public Double geodist(String key, String member1, String member2) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geodist( key,  member1,  member2);
    //     }
    // }
    //
    // @Override
    // public Double geodist(String key, String member1, String member2, GeoUnit unit) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geodist( key,  member1,  member2,  unit);
    //     }
    // }
    //
    // @Override
    // public List<String> geohash(String key, String... members) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geohash( key,  members);
    //     }
    // }
    //
    // @Override
    // public List<GeoCoordinate> geopos(String key, String... members) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.geopos( key,  members);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadius(String key, double longitude, double latitude, double radius, GeoUnit unit) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadius( key,  longitude,  latitude,  radius,  unit);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusReadonly(String key, double longitude, double latitude, double radius, GeoUnit unit) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusReadonly( key,  longitude,  latitude,  radius,  unit);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadius(String key, double longitude, double latitude, double radius, GeoUnit unit, GeoRadiusParam param) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadius( key,  longitude,  latitude,  radius,  unit,   param);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusReadonly(String key, double longitude, double latitude, double radius, GeoUnit unit, GeoRadiusParam param) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusReadonly( key,  longitude,  latitude,  radius,  unit,   param);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusByMember(String key, String member, double radius, GeoUnit unit) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusByMember( key,  member,  radius,  unit);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusByMemberReadonly(String key, String member, double radius, GeoUnit unit) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusByMemberReadonly( key,  member,  radius,  unit);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusByMember(String key, String member, double radius, GeoUnit unit, GeoRadiusParam param) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusByMember( key,  member,  radius,  unit,   param);
    //     }
    // }
    //
    // @Override
    // public List<GeoRadiusResponse> georadiusByMemberReadonly(String key, String member, double radius, GeoUnit unit, GeoRadiusParam param) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.georadiusByMemberReadonly( key,  member,   radius,   unit,   param);
    //     }
    // }
    //
    // @Override
    // public List<Long> bitfield(String key, String... arguments) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.bitfield( key,   arguments) ;
    //     }
    // }
    //
    // @Override
    // public Long hstrlen(String key, String field) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.hstrlen( key,  field);
    //     }
    // }
    //
    // @Override
    // public StreamEntryID xadd(String key, StreamEntryID id, Map<String, String> hash) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xadd( key,   id,   hash) ;
    //     }
    // }
    //
    // @Override
    // public StreamEntryID xadd(String key, StreamEntryID id, Map<String, String> hash, long maxLen, boolean approximateLength) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xadd( key,   id,   hash,  maxLen,  approximateLength);
    //     }
    // }
    //
    // @Override
    // public Long xlen(String key) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xlen( key);
    //     }
    // }
    //
    // @Override
    // public List<StreamEntry> xrange(String key, StreamEntryID start, StreamEntryID end, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xrange( key,   start,   end,  count);
    //     }
    // }
    //
    // @Override
    // public List<StreamEntry> xrevrange(String key, StreamEntryID end, StreamEntryID start, int count) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xrevrange( key,   end,   start,  count);
    //     }
    // }
    //
    // @Override
    // public long xack(String key, String group, StreamEntryID... ids) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xack(  key,   group,  ids);
    //     }
    // }
    //
    // @Override
    // public String xgroupCreate(String key, String groupname, StreamEntryID id, boolean makeStream) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xgroupCreate( key,  groupname,   id,  makeStream);
    //     }
    // }
    //
    // @Override
    // public String xgroupSetID(String key, String groupname, StreamEntryID id) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xgroupSetID( key,  groupname,   id);
    //     }
    // }
    //
    // @Override
    // public long xgroupDestroy(String key, String groupname) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xgroupDestroy( key,  groupname);
    //     }
    // }
    //
    // @Override
    // public String xgroupDelConsumer(String key, String groupname, String consumername) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xgroupDelConsumer( key,  groupname,  consumername);
    //     }
    // }
    //
    // @Override
    // public List<StreamPendingEntry> xpending(String key, String groupname, StreamEntryID start, StreamEntryID end, int count, String consumername) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xpending( key,  groupname,   start,   end,  count,  consumername);
    //     }
    // }
    //
    // @Override
    // public long xdel(String key, StreamEntryID... ids) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xdel( key,  ids);
    //     }
    // }
    //
    // @Override
    // public long xtrim(String key, long maxLen, boolean approximate) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xtrim( key,  maxLen,  approximate);
    //     }
    // }
    //
    // @Override
    // public List<StreamEntry> xclaim(String key, String group, String consumername, long minIdleTime, long newIdleTime, int retries, boolean force, StreamEntryID... ids) {
    //     try (ShardedJedis j = pool.getResource()) {
    //         return j.xclaim( key,  group,  consumername,  minIdleTime,  newIdleTime,  retries,  force,  ids) ;
    //     }
    // }
}

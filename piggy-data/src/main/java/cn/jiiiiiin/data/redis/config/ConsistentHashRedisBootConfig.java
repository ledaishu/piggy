package cn.jiiiiiin.data.redis.config;

// import cn.jiiiiiin.data.redis.client.ConsistentHashRedis;
// import java.util.ArrayList;
// import java.util.List;
// import lombok.var;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import redis.clients.jedis.JedisPoolConfig;
// import redis.clients.jedis.JedisShardInfo;

/**
 * @see cn.jiiiiiin.data.config.RedisConfig
 * 一致性哈希 springboot 配置
 * @author zetee
 */
// @Configuration
public class ConsistentHashRedisBootConfig {

    // /**
    //  *ip:port,weght;ip:port,weght;ip:port,weght
    //  */
    // @Value("${jedis.shard.host:}")
    // private String jedisShardHost;
    // /**
    //  * 超时
    //  */
    // @Value("${jedis.shard.timeout:2000}")
    // private int timeout;
    // /**
    //  * #最大连接数
    //  */
    // @Value("${jedis.shard.max-total:2000}")
    // private int maxTotal;
    // /**
    //  * #最大空闲数
    //  */
    // @Value("${jedis.shard.max-idle:1000}")
    // private int maxIdle;
    // /**
    //  * #最小空闲数
    //  */
    // @Value("${jedis.shard.min-idle:30}")
    // private int minIdle;
    // /**
    //  * #效验使用可用连接
    //  */
    // @Value("${jedis.shard.test-on-borrow:false}")
    // private boolean testOnBorrow;
    // /**
    //  * #效验归还可用连接
    //  */
    // @Value("${jedis.shard.test-on-return:false}")
    // private boolean testOnReturn;
    // @Bean
    // public ConsistentHashRedis consistentHashRedis() {
    //     if(jedisShardHost.length()==0) {
    //         return null;
    //     }
    //     var hosts= jedisShardHost.split(";");
    //     List<JedisShardInfo> shards=new ArrayList<>();
    //     for (var host : hosts) {
    //         var urlWs= host.split(",");
    //         String hostPort=host.trim();
    //         int weight=1;
    //         if(urlWs.length>1){
    //             hostPort=urlWs[0].trim();
    //             weight=Integer.parseInt(urlWs[1].trim());
    //         }
    //         var ipPortSplit=hostPort.split(":");
    //         String ipAddress=ipPortSplit[0].trim();
    //         int port=Integer.parseInt( ipPortSplit[1].trim());
    //         var jediShardInfo=new JedisShardInfo(ipAddress,port,timeout,timeout,weight);
    //         shards.add(jediShardInfo);
    //     }
    //
    //     JedisPoolConfig poolConfig = new JedisPoolConfig();
    //     poolConfig.setMaxTotal(maxTotal);//每一个redis 服务器端,一共有 ${maxTotal}
    //     poolConfig.setMaxIdle(maxIdle);//每一个redis 服务器端,最大空闲连接数=${MaxIdle}
    //     poolConfig.setMinIdle(minIdle);
    //     poolConfig.setMaxWaitMillis(timeout);
    //     poolConfig.setTestOnBorrow(testOnBorrow);
    //     poolConfig.setTestOnReturn(testOnReturn);
    //     ConsistentHashRedis consistentHashRedis=new ConsistentHashRedis(poolConfig,shards);
    //     return  consistentHashRedis;
    // }
}
package cn.jiiiiiin.data.utils.bean;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @param <T> the type of the input to the function
 * @param <R> the type of the result of the function
 * @author zhaojun
 */
@FunctionalInterface
public interface Fn<T, R> extends Function<T, R>, Serializable {

}

package cn.jiiiiiin.data.utils.bean;

import cn.jiiiiiin.data.exception.DataException;
import cn.jiiiiiin.data.exception.DataException.ERR;
import java.beans.Introspector;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * https://juejin.cn/post/6854573210928939016
 *
 * @author zhaojun
 */
public class Reflections {

  private static final Pattern GET_PATTERN = Pattern.compile("^get[A-Z].*");
  private static final Pattern IS_PATTERN = Pattern.compile("^is[A-Z].*");

  private Reflections() {
  }

  /**
   * 获取字段名，通过对于bean的get/set fn
   * @param fn get/set fn
   * @return 字段名
   */
  public static String fnToFieldName(Fn fn) {
    try {
      Method method = fn.getClass().getDeclaredMethod("writeReplace");
      method.setAccessible(Boolean.TRUE);
      SerializedLambda serializedLambda = (SerializedLambda) method.invoke(fn);
      String getter = serializedLambda.getImplMethodName();
      if (GET_PATTERN.matcher(getter).matches()) {
        getter = getter.substring(3);
      } else if (IS_PATTERN.matcher(getter).matches()) {
        getter = getter.substring(2);
      }
      return Introspector.decapitalize(getter);
    } catch (ReflectiveOperationException e) {
      throw new DataException(ERR.FN2FIELD_NAME_ERR, e);
    }
  }
}

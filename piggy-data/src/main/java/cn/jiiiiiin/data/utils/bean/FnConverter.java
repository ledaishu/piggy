package cn.jiiiiiin.data.utils.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.val;
import org.apache.commons.lang3.StringUtils;

/**
 * zhaojun
 */
@Deprecated
public final class FnConverter<T, R> {

  /**
   * 传入方法返回字段名
   *
   * @param fn get/set fn
   * @return 字段名
   */
  public String fnToFieldName(Fn<T, R> fn) {
    return Reflections.fnToFieldName(fn);
  }

  /**
   * 获取对于bean的表对应字段，将驼峰转换成下划线分词
   *
   * @param fn get/set fn
   * @return 表字段名
   */
  public String fnToDBFieldName(Fn<T, R> fn) {
    val fieldName = Reflections.fnToFieldName(fn);
    return camel2Underline(fieldName);
  }

  /**
   * 参考： https://blog.csdn.net/yelllowcong/article/details/78481335
   *
   * @param line 待转换驼峰字符串
   * @return 转换后的下划线字符串
   */
  public static String camel2Underline(String line) {
    if (line == null || "".equals(line)) {
      return "";
    }
    line = String.valueOf(line.charAt(0)).toUpperCase()
        .concat(line.substring(1));
    StringBuilder sb = new StringBuilder();
    Pattern pattern = Pattern.compile("[A-Z]([a-z\\d]+)?");
    Matcher matcher = pattern.matcher(line);
    while (matcher.find()) {
      String word = matcher.group();
      sb.append(word.toUpperCase());
      sb.append(matcher.end() == line.length() ? "" : "_");
    }
    return sb.toString();
  }
}

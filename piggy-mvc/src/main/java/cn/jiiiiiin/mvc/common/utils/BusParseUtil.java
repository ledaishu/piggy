package cn.jiiiiiin.mvc.common.utils;


//import cn.jiiiiiin.mvc.common.api.R;
//import cn.jiiiiiin.mvc.common.exception.ApiErrorCode;
//import cn.jiiiiiin.mvc.common.exception.ApiException;
//import java.util.function.Consumer;
//import java.util.function.Predicate;
//import lombok.val;

/**
 * 业务异常辅助解析工具类
 *
 * @author jiiiiiin
 */
@Deprecated
public interface BusParseUtil {

  ///**
  // * 根据给定的参数 predicate判断是否为业务级别错误，如果是则抛出异常，即业务错误，如果业务成功则调用consumer进行消费{@link R#getData()}
  // */
  //static void check(R<Object> data, Predicate<R<Object>> predicate, Consumer<Object> consumer) {
  //  if (predicate.test(data)) {
  //    consumer.accept(data.getData());
  //  }
  //}
  //
  ///**
  // * 如果data中code非{@link ApiErrorCode#SUCCESS}则抛出异常，即业务错误，如果业务成功则调用consumer进行消费{@link R#getData()}
  // */
  //static void check(R<Object> data, Consumer<Object> consumer) {
  //  check(data, BusParseUtil::checkAndHandlerErr, consumer);
  //}
  //
  ///**
  // * 如果data中code非{@link ApiErrorCode#SUCCESS}则抛出异常，即业务错误，否则返回true
  // */
  //static boolean checkAndHandlerErr(R r) {
  //  val flag = check(r);
  //  if (!flag) {
  //    throw new ApiException(r.getMsg(), r.getCode());
  //  }
  //  return true;
  //}
  //
  ///**
  // * 检查data中code是否等于{@link ApiErrorCode#SUCCESS}
  // *
  // * @param r 带检查响应对象
  // * @return 是否为业务成功响应状态
  // */
  //static boolean check(R r) {
  //  return r.getCode() == ApiErrorCode.SUCCESS.getCode();
  //}

}

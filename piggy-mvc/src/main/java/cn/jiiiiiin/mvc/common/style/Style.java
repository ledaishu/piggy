package cn.jiiiiiin.mvc.common.style;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import cn.hutool.core.util.ObjectUtil;
import cn.jiiiiiin.mvc.common.exception.ApiException;
import cn.jiiiiiin.mvc.common.style.chain.IChain;
import cn.jiiiiiin.mvc.common.style.chain.annotaion.Chain;
import cn.jiiiiiin.mvc.common.style.chain.annotaion.Completion;
import cn.jiiiiiin.mvc.common.style.chain.annotaion.Handler;
import cn.jiiiiiin.mvc.common.style.chain.annotaion.Transform;
import cn.jiiiiiin.mvc.common.style.chain.annotaion.Validator;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

/**
 * @author jiiiiiin
 */
@Documented
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface Style {

  Class<? extends IChain>[] chains() default {};

  @Slf4j
  class Util {

    /**
     * 执行style中的chains中每一个chain的validate方法
     *
     * @param request            {@link HttpServletRequest}
     * @param response           {@link HttpServletRequest}
     * @param handler            参考spring mvc拦截器相关知识
     * @param chains             {@link IChain}
     * @param applicationContext {@link ApplicationContext}
     * @return 如果有1个chain校验不通过，则整个流程不会往下执行
     */
    public static boolean callValidates(HttpServletRequest request,
        HttpServletResponse response,
        Object handler, Class<? extends IChain>[] chains, ApplicationContext applicationContext)
        throws ApiException {
      for (final Class<? extends IChain> chainClass : chains) {
        val vAnnotation = chainClass.getAnnotation(Validator.class);
        val cAnnotation = chainClass.getAnnotation(Chain.class);
        // 校验该bean是否为规范注解
        if (!ObjectUtil.isNull(vAnnotation) || !ObjectUtil.isNull(cAnnotation)) {
          val chain = getChainBean(applicationContext, chainClass);
          // 执行校验逻辑
          val res = chain.validate(request, response, handler);
          // 如果有1个chain校验不通过，则整个流程不会往下执行
          if (!res) {
            return false;
          }
        }
      }
      return true;
    }

    /**
     * 调用声明的转换器群组
     *
     * @param request            {@link HttpServletRequest}
     * @param response           {@link HttpServletRequest}
     * @param handler            参考spring mvc拦截器相关知识
     * @param chains             {@link IChain}
     * @param applicationContext {@link ApplicationContext}
     */
    public static void callTransforms(HttpServletRequest request, HttpServletResponse response,
        Object handler, Class<? extends IChain>[] chains, ApplicationContext applicationContext) {
      for (final Class<? extends IChain> chainClass : chains) {
        val vAnnotation = chainClass.getAnnotation(Transform.class);
        val cAnnotation = chainClass.getAnnotation(Chain.class);
        // 校验该bean是否为规范注解
        if (!ObjectUtil.isNull(vAnnotation) || !ObjectUtil.isNull(cAnnotation)) {
          val chain = getChainBean(applicationContext, chainClass);
          // 执行校验逻辑
          chain.transform(request, response, handler);
        }
      }
    }

    private static <T extends IChain> T getChainBean(ApplicationContext applicationContext,
        Class<T> aClass) {
      return getBean4ClassSimpleName(applicationContext, aClass);
    }

    private static <T extends IChain> T getBean4ClassSimpleName(
        ApplicationContext applicationContext,
        @NonNull Class<T> aClass) {
      try {
        val simpleName = aClass.getSimpleName();
        val beanName = StringUtils.lowerCase(String.valueOf(simpleName.charAt(0)))
            .concat(simpleName.substring(1));
        return (T) applicationContext.getBean(beanName);
      } catch (NoSuchBeanDefinitionException e) {
        throw new ApiException(String.format("找不到%s对应的bean", aClass.getSimpleName()), e);
      }
    }

    /**
     * 1. preHandle 方法的返回值为true 时才能被调用
     *
     * @param request            {@link HttpServletRequest}
     * @param response           {@link HttpServletRequest}
     * @param handler            参考spring mvc拦截器相关知识
     * @param chains             {@link IChain}
     * @param applicationContext {@link ApplicationContext}
     */
    public static void callHandlers(HttpServletRequest request,
        HttpServletResponse response, Object handler, Class<? extends IChain>[] chains,
        ApplicationContext applicationContext) throws ApiException {
      // postHandle 方法被调用的方向跟preHandle 是相反的，也就是说先声明的Interceptor 的postHandle 方法反而会后执行
      for (int i = chains.length - 1; i >= 0; i--) {
        val chainClass = chains[i];
        val hAnnotation = chainClass.getAnnotation(Handler.class);
        val cAnnotation = chainClass.getAnnotation(Chain.class);
        // 校验该bean是否为规范注解
        if (!ObjectUtil.isNull(hAnnotation) || !ObjectUtil.isNull(cAnnotation)) {
          val chain = getChainBean(applicationContext, chainClass);
          chain.handler(request, response, handler);
        }
      }
    }

    /**
     * 1. preHandle 方法的返回值为true 时才能被调用
     * <p>
     * 2. 该方法会在整个请求处理完成，也就是在视图返回并被渲染之后执行。所以在该方法中可以进行资源的释放操作。
     * <p>
     * 3. 如果在Controller 中抛出的异常已经被Spring 的异常处理器给处理了的话，那么这个异常对象就是是null 。
     *
     * @param request            {@link HttpServletRequest}
     * @param response           {@link HttpServletRequest}
     * @param handler            参考spring mvc拦截器相关知识
     * @param chains             {@link IChain}
     * @param applicationContext {@link ApplicationContext}
     */
    public static void callCompleter(HttpServletRequest request,
        HttpServletResponse response, Object handler, Class<? extends IChain>[] chains,
        ApplicationContext applicationContext) {
      for (int i = chains.length - 1; i >= 0; i--) {
        val chainClass = chains[i];
        val hAnnotation = chainClass.getAnnotation(Completion.class);
        val cAnnotation = chainClass.getAnnotation(Chain.class);
        // 校验该bean是否为规范注解
        if (!ObjectUtil.isNull(hAnnotation) || !ObjectUtil.isNull(cAnnotation)) {
          val chain = getChainBean(applicationContext, chainClass);
          chain.completed(request, response, handler);
        }
      }
    }

  }
}

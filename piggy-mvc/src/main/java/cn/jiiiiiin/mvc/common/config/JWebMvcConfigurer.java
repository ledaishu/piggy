package cn.jiiiiiin.mvc.common.config;

import cn.jiiiiiin.mvc.common.properties.MVCProperties;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 1.添加Style业务拦截器
 *
 * @author jiiiiiin
 */
@Configuration
//@AllArgsConstructor
@Slf4j
@EnableConfigurationProperties(MVCProperties.class)
@Order(Integer.MAX_VALUE)
public class JWebMvcConfigurer implements WebMvcConfigurer {

  //private final ApplicationContext applicationContext;
  //
  //@Override
  //public void addInterceptors(InterceptorRegistry registry) {
  //  registry
  //      .addInterceptor(new StyleHandlerInterceptor(applicationContext))
  //      .addPathPatterns("/**");
  //}

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    // 解决通用响应处理中如果返回的是一个字符串类型，但是返回的是null导致底层转换出错的问题
    // https://stackoverflow.com/questions/44121648/controlleradvice-responsebodyadvice-failed-to-enclose-a-string-response
    converters.add(0, new MappingJackson2HttpMessageConverter());
  }
}

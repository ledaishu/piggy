package cn.jiiiiiin.mvc.common.properties;

import cn.jiiiiiin.mvc.common.style.Style;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author jiiiiiin
 */
@ConfigurationProperties(prefix = "jiiiiiin.mvc")
@Setter
@Getter
@NoArgsConstructor
@RefreshScope
@ToString
@Slf4j
@Component("mvcProperties")
public class MVCProperties {

  /**
   * 配置需要被校验的{@link Style}注解及其子注解
   */
  @Deprecated
  private List<String> styles = new LinkedList<>();

  @PostConstruct
  private void initialize() {
    styles.add(Style.class.getName());
    styles.add("cn.jiiiiiin.business.style.ConfirmStyle");
    log.info("MVCProperties initialized - styles: {}", styles);
  }

}

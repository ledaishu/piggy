package cn.jiiiiiin.mvc.common.style.chain;

import cn.jiiiiiin.mvc.common.exception.ApiException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 一般用于对控制器进行环绕处理，springmvc拦截器的一个简单包装
 * <p>
 * 1.一般定义在业务应用的`**.style.chain`包下
 * <p>
 * 2.实现{@link IChain#validate(HttpServletRequest, HttpServletResponse, Object)}等接口，完成环绕校验
 *
 * @author jiiiiiin
 */
public interface IChain {

  /**
   * 在控制器方法执行前调用
   * <p>
   * Interceptor 中的preHandle 方法，所以可以在这个方法中进行一些前置初始化操作或者是对当前请求的一个预处理，也可以在这个方法中进行一些判断来决定请求是否要继续进行下去。该方法的返回值是布尔值Boolean类型的，当它返回为false
   * 时，表示请求结束，后续的Interceptor 和Controller 都不会再执行；当返回值为true 时就会继续调用下一个Interceptor 的preHandle
   * 方法，如果已经是最后一个Interceptor 的时候就会是调用当前请求的Controller 方法。
   * <p>
   * 1.如果返回false，则必须抛出异常，以响应为什么请求会被截断，否则前端将收不到任何结果
   *
   * @param request  {@link HttpServletRequest}
   * @param response {@link HttpServletRequest}
   * @param handler  参考spring mvc拦截器相关知识
   * @return 返回true流程继续，如果有1个chain校验不通过，则整个流程不会往下执行
   * @throws ApiException 抛出异常则流程也被断开，比如在校验发现业务问题的时候就可以抛出异常，响应给前端
   */
  default boolean validate(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws ApiException {
    return true;
  }


  /**
   * @param request  {@link HttpServletRequest}
   * @param response {@link HttpServletRequest}
   * @param handler  参考spring mvc拦截器相关知识
   * @throws ApiException 转换失败如不符合转换条件要求，按需抛出异常终端请求
   * @see IChain#validate(HttpServletRequest, HttpServletResponse, Object)
   * 执行成功之后接着被调用，用于对参数进行一些转换操作，如将前端传递的密文转换成明文，直接传递到控制器处理
   */
  default void transform(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws ApiException {
  }

  /**
   * 在控制器方法方法后，视图渲染前调用
   * <p>
   * 在当前请求进行处理之后，也就是Controller 方法调用之后执行，但是它会在DispatcherServlet 进行视图返回渲染之前被调用，所以我们可以在这个方法中对Controller
   * 处理之后的ModelAndView 对象进行操作
   * <p>
   * 1. preHandle 方法的返回值为true 时才能被调用
   *
   * @param request  {@link HttpServletRequest}
   * @param response {@link HttpServletRequest}
   * @param handler  参考spring mvc拦截器相关知识
   * @throws ApiException 抛出异常则流程也被手动破坏，异常错误消息响应给前端
   */
  default void handler(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws ApiException {
  }

  /**
   * 1. preHandle 方法的返回值为true 时才能被调用
   * <p>
   * 2. 该方法会在整个请求处理完成，也就是在视图返回并被渲染之后执行。所以在该方法中可以进行资源的释放操作。
   *
   * @param request  {@link HttpServletRequest}
   * @param response {@link HttpServletRequest}
   * @param handler  参考spring mvc拦截器相关知识
   */
  default void completed(HttpServletRequest request, HttpServletResponse response, Object handler) {
  }
}

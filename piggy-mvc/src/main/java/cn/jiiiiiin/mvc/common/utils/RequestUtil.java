package cn.jiiiiiin.mvc.common.utils;

import static cn.jiiiiiin.mvc.common.exception.ApiErrorCode.GET_RESP_ERR;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

import cn.hutool.core.util.StrUtil;
import cn.jiiiiiin.mvc.common.exception.ApiErrorCode;
import cn.jiiiiiin.mvc.common.exception.ApiException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

/**
 * @author jiiiiiin
 */
@Slf4j
public class RequestUtil {

  /**
   * 一般启动
   *
   * @param args           {@link SpringApplication}
   * @param primarySources {@link SpringApplication}
   */
  public static void run(String[] args, Class<?>... primarySources) {
    val app = new SpringApplication(primarySources);
    app.setBannerMode(Banner.Mode.OFF);
    app.run(args);
  }


  /**
   * 获取 HttpServletRequest，注意开发人员要慎用！
   * <p>
   * https://www.jianshu.com/p/80165b7743cf
   *
   * @return {HttpServletRequest}
   */
  public static HttpServletRequest getRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
      return servletRequestAttributes.getRequest();
    } else {
      throw new ApiException(ApiErrorCode.GET_REQUEST_ERR);
    }
  }

  /**
   * @return {@link HttpServletResponse}
   */
  public static HttpServletResponse getResponse() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
      return servletRequestAttributes.getResponse();
    } else {
      throw new ApiException(GET_RESP_ERR);
    }
  }

  /**
   * 获取指定key对应的参数paramName，如果获取不到则抛出异常
   * <p>
   * 1.现获取请求参数
   * <p>
   * 2.获取请求头
   *
   * @param key       待获取的key标示
   * @param paramName key对应的描述
   * @return 获取到的请求参数｜请求头｜请求属性其中之一的值
   */
  public static String getData(@NonNull String key, @NonNull String paramName) {
    String res = _getVal(key);
    if (StringUtils.isBlank(res)) {
      throw new ApiException("参数 " + paramName + "[" + key + "] 为空错误", ApiErrorCode.FAILED.getCode());
    }
    return res.trim();
  }

  /**
   * @param key    待获取的key标示
   * @param defVal 如果没有从{{@link #getData(WebRequest, String, String)}} 类似范围中获取到值，就使用该默认值返回
   * @return 获取到的请求参数｜请求头｜请求属性其中之一的值
   */
  public static String getDataWithDef(@NonNull String key, @NonNull String defVal) {
    String res = _getVal(key);
    if (StringUtils.isBlank(res)) {
      log.debug("没有获取到 key {} 对应的val，使用默认值 {}", key, defVal);
    }
    if (StrUtil.isEmpty(res)) {
      res = defVal;
    }
    return res.trim();
  }

  private static String _getVal(@NonNull String key) {
    HttpServletRequest request = getRequest();
    String res;
    res = request.getParameter(key);
    if (StringUtils.isBlank(res)) {
      res = request.getHeader(key);
    }
    // 这种设值只会在特殊时候使用，如在统一次请求链路中服务器端用这个容器做一个跳板
    if (StringUtils.isBlank(res)) {
      Object temp = request.getAttribute(key);
      if (ObjectUtils.isNotEmpty(temp)) {
        res = String.valueOf(temp);
      }
    }
    return res;
  }

  /**
   * @param key       待获取的key标示
   * @param paramName key对应的描述
   * @return 获取到的请求参数｜请求头｜请求属性其中之一的值，或者返回null
   */
  public static String getDataOrNull(String key, String paramName) {
    String res = _getVal(key);
    if (StringUtils.isBlank(res)) {
      log.debug("参数 " + paramName + "[" + key + "] 为空错误");
    } else {
      res = res.trim();
    }
    return res;
  }


  /**
   * 获取指定key对应的参数paramName，如果获取不到则抛出异常
   *
   * @param request   {@link WebRequest}
   * @param key       待获取的key标示
   * @param paramName key对应的描述
   * @return 获取到的请求参数｜请求头｜请求属性其中之一的值，或者返回null
   */
  public static String getData(WebRequest request, String key, String paramName) {
    String res;
    res = request.getParameter(key);
    if (StringUtils.isBlank(res)) {
      res = request.getHeader(key);
    }
    if (StringUtils.isBlank(res)) {
      Object temp = request.getAttribute(key, SCOPE_REQUEST);
      if (ObjectUtils.isNotEmpty(temp)) {
        res = String.valueOf(temp);
      }
    }
    if (StringUtils.isBlank(res)) {
      throw new ApiException("参数 " + paramName + "[" + key + "] 为空错误", ApiErrorCode.FAILED.getCode());
    }
    return res.trim();
  }
}

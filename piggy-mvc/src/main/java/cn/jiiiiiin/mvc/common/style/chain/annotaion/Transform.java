package cn.jiiiiiin.mvc.common.style.chain.annotaion;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Transform {

    String[] value() default {};
}

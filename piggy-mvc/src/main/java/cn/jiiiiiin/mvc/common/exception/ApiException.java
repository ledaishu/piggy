package cn.jiiiiiin.mvc.common.exception;

import lombok.Getter;
import lombok.NonNull;

/**
 * 不同的模块使用自己的异常类，并基础当前类
 *
 * @author jiiiiiin
 */
@Getter
public class ApiException extends RuntimeException {

  private long code = ApiErrorCode.FAILED.getCode();

  public ApiException(String message, Exception e) {
    super(message, e);
  }

  public ApiException(@NonNull String message, long code) {
    super(message);
    this.code = code;
  }

  public ApiException(@NonNull String message, long code, @NonNull Exception e) {
    super(message, e);
    this.code = code;
  }

  public ApiException(@NonNull ApiErrorCode errorCode,@NonNull Exception e) {
    super(errorCode.getMsg(), e);
    this.code = errorCode.getCode();
  }

  public ApiException(@NonNull ApiErrorCode errorCode) {
    super(errorCode.getMsg());
    this.code = errorCode.getCode();
  }
}

package cn.jiiiiiin.mvc.common.aspect;

/**
 * https://www.ibm.com/developerworks/cn/java/j-spring-boot-aop-web-log-processing-and-distributed-locking/index.html
 *
 * @author jiiiiiin
 */
//@Aspect
//@Component
//@Order(33)
//@Slf4j
//@AllArgsConstructor
@Deprecated
public class StyleAspectByService {

  //private final ApplicationContext applicationContext;
  //
  //// @Pointcut("execution(* cn.jiiiiiin..service..*.*(..))")
  //@Pointcut("execution(* cn.jiiiiiin..service..*.*(..))")
  //public void pointcut() {
  //    log.debug("TODO 需要动态修改拦截规则");
  //}
  //
  //@Around("pointcut() && @annotation(style)")
  //public Object doAround(ProceedingJoinPoint joinPoint, Style style) throws Throwable {
  //  Object obj;
  //  // 注意：在使用风格链的时候必须心里面要清楚第三个参数是和配置的“层”相关
  //  val breakFlag = Style.Util
  //      .callValidates(MVCUtil.getRequest(), MVCUtil.getResponse(), joinPoint, style.chains(),
  //          applicationContext);
  //  Style.Util.callTransforms(MVCUtil.getRequest(), MVCUtil.getResponse(), joinPoint, style.chains(),
  //          applicationContext);
  //  if (breakFlag) {
  //    try {
  //      obj = joinPoint.proceed();
  //      Style.Util
  //          .callHandlers(MVCUtil.getRequest(), MVCUtil.getResponse(), joinPoint, style.chains(),
  //              applicationContext);
  //      return obj;
  //
  //    } catch (Throwable throwable) {
  //      log.error("StyleAspectByService#doAround 执行切入点方法出错", throwable);
  //      throw throwable;
  //    } finally {
  //      Style.Util
  //          .callCompleter(MVCUtil.getRequest(), MVCUtil.getResponse(), joinPoint, style.chains(),
  //              applicationContext);
  //    }
  //  } else {
  //    throw new MVCException("执行[StyleChains]出错，doAround没有正常执行");
  //  }
  //}
  //
  //
}

package cn.jiiiiiin.mvc.common.dict;

import lombok.Getter;

/**
 * @author jiiiiiin
 */

@Getter
public enum MVCErr {
  MISSING_SERVLET_REQUEST_PARAMETER_EXCEPTION(-2000, "缺少 [%s] 参数错误"),
  CONSTRAINT_VIOLATION_EXCEPTION(-2001, "JSR303参数校验异常"),
  PERSISTENCE_EXCEPTION(-2002, "数据库操作错误")
  ;
  int code;
  String msg;

  MVCErr(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }
}

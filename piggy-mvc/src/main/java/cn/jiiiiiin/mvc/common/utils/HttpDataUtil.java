package cn.jiiiiiin.mvc.common.utils;

import cn.jiiiiiin.mvc.common.dict.CommonConstants;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.NonNull;
import org.springframework.http.HttpStatus;

/**
 * @author jiiiiiin
 */
public class HttpDataUtil {

  /**
   * 目前还不能达到RESTFul要求，还是是通过 {@link cn.jiiiiiin.mvc.common.api.R} 对象来进行业务错误判断，所以返回状态码为 200
   *
   * @param response {@link HttpServletResponse}
   * @param jsonStr  需要响应的json字符串
   * @throws IOException 如果响应返回数据失败
   */
  public static void respJson(@NonNull HttpServletResponse response, @NonNull String jsonStr)
      throws IOException {
    response.setStatus(HttpStatus.OK.value());
    response.setContentType(CommonConstants.CONTENT_TYPE_JSON);
    response.getWriter().write(jsonStr);
  }
}

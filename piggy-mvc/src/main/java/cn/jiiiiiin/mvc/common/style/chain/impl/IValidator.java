package cn.jiiiiiin.mvc.common.style.chain.impl;

import cn.jiiiiiin.mvc.common.style.chain.IChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 一般用于进入控制器之前的校验通用业务逻辑抽象
 *
 * 0.bean定义使用{@link cn.jiiiiiin.mvc.common.style.chain.annotaion.Validator}声明
 *
 * 1.一般定义在业务应用的`**.style.validator`包下
 *
 * 2.只实现{@link IChain#validate(HttpServletRequest, HttpServletResponse, Object)}接口
 *
 * @author jiiiiiin
 */
public interface IValidator extends IChain {

}

package cn.jiiiiiin.mvc.common.dict;

/**
 * @author jiiiiiin
 */
@Deprecated
public interface MVCDict {

    /**
     * 请求中标识渠道的参数key
     */
    String CHANNEL = "channel";

    /**
     * 全局异常消息
     * 废弃
     */
    @Deprecated
    String GLOB_UNDEFINED_EXCEPTION = "网络异常，请稍后再试！[%s]";

}

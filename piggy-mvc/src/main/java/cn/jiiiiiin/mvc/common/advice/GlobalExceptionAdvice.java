package cn.jiiiiiin.mvc.common.advice;

/**
 * 统一异常处理
 *
 * 不同应用根据自己的需要参考该类实现
 *
 * @author jiiiiiin
 */
@Deprecated
// @RestControllerAdvice
//@Slf4j
//@Order(Integer.MAX_VALUE)
public class GlobalExceptionAdvice {

  ///**
  // * 输出错误消息最迟的字符数
  // */
  //private static final int MAX_OUTPUT_ERRMSG = 100;
  //
  //@ExceptionHandler(value = MVCException.class)
  //public R<MVCException> handlerBusinessErrException(HttpServletRequest req, MVCException ex) {
  //  _le(req, ex);
  //  return restResult(ex, ex.getCode(), ex.getMessage());
  //}
  //
  //@ExceptionHandler(value = ConstraintViolationException.class)
  //public R<Exception> handlerConstraintViolationException(HttpServletRequest req, Exception ex) {
  //  _le(req, ex);
  //  val constraintViolationException = (ConstraintViolationException) ex;
  //  StringBuilder stringBuilder = new StringBuilder();
  //  var constraintViolations = constraintViolationException.getConstraintViolations().iterator();
  //  while (constraintViolations.hasNext()) {
  //    var violation = constraintViolations.next();
  //    stringBuilder.append(violation.getMessage()).append("[").append(violation.getPropertyPath())
  //        .append("]").append(" ");
  //  }
  //  return restResult(null, MVCErr.CONSTRAINT_VIOLATION_EXCEPTION.getCode(),
  //      stringBuilder.toString().trim());
  //}
  //
  //@ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
  //public R<List<FieldError>> handlerMethodArgumentNotValidException(HttpServletRequest req,
  //    MethodArgumentNotValidException ex) {
  //  _le(req, ex);
  //  BindingResult bindingResult = ex.getBindingResult();
  //  StringBuilder stringBuilder = new StringBuilder();
  //  var iterator = bindingResult.getFieldErrors().iterator();
  //  while (iterator.hasNext()) {
  //    var fieldError = iterator.next();
  //    stringBuilder.append(fieldError.getDefaultMessage()).append("[").append(fieldError.getField())
  //        .append("]").append(" ");
  //  }
  //  return restResult(bindingResult.getFieldErrors(), ApiErrorCode.FAILED.getCode(),
  //      stringBuilder.toString().trim());
  //}
  //
  //
  //@ExceptionHandler(value = MissingServletRequestParameterException.class)
  //public R<Exception> handlerMissingServletRequestParameterException(HttpServletRequest req,
  //    Exception ex) {
  //  _le(req, ex);
  //  val missingServletRequestParameterException = (MissingServletRequestParameterException) ex;
  //  return restResult(ex, MVCErr.MISSING_SERVLET_REQUEST_PARAMETER_EXCEPTION.getCode(),
  //      String.format(MVCErr.MISSING_SERVLET_REQUEST_PARAMETER_EXCEPTION.getMsg(),
  //          missingServletRequestParameterException.getParameterName()));
  //}
  //
  //
  //@ExceptionHandler(value = LintException.class)
  //public R<Exception> handlerValidationException(HttpServletRequest req, Exception ex) {
  //  _le(req, ex);
  //  var message = ex.getMessage();
  //  if (ExceptionUtil.isFromOrSuppressedThrowable(ex, LintException.class)) {
  //    val lintException = ExceptionUtil.convertFromOrSuppressedThrowable(ex, LintException.class);
  //    message = lintException.getMessage();
  //    return restResult(ex, ApiErrorCode.FAILED.getCode(), message);
  //  }
  //  return restResult(ex, ApiErrorCode.FAILED.getCode(), String.format("参数校验出错 %s", message));
  //}
  //
  //@ExceptionHandler(value = org.springframework.security.access.AccessDeniedException.class)
  //public R<Exception> handlerAccessDeniedException(HttpServletRequest req, Exception ex) {
  //  _le(req, ex);
  //  val accessDeniedException = (org.springframework.security.access.AccessDeniedException) ex;
  //  return restResult(ex, GlobalExceptionCodeDict.SS_ACCESS_DENIED_EXCEPTION.getCode(),
  //      ex.getMessage());
  //}
  //
  //private R<Exception> getExceptionR(Exception ex, String message) {
  //  if (message.length() > MAX_OUTPUT_ERRMSG) {
  //    return restResult(ex, ApiErrorCode.FAILED.getCode(),
  //        String.format(MVCDict.GLOB_UNDEFINED_EXCEPTION, message.substring(0, MAX_OUTPUT_ERRMSG)));
  //  } else {
  //    return restResult(ex, ApiErrorCode.FAILED.getCode(),
  //        String.format(MVCDict.GLOB_UNDEFINED_EXCEPTION, message));
  //  }
  //}
  //
  //@ExceptionHandler(value = PersistenceException.class)
  //public R<Exception> handlerPersistenceException(HttpServletRequest req, Exception ex) {
  //  _le(req, ex);
  //  final PersistenceException persistenceException = (PersistenceException) ex;
  //  return restResult(persistenceException, MVCErr.PERSISTENCE_EXCEPTION.getCode(),
  //      MVCErr.PERSISTENCE_EXCEPTION.getMsg());
  //}
  //
  ///**
  // * 不同应用自己根据需要实现
  // * @param req
  // * @param ex
  // * @return
  // */
  //@Deprecated
  //@ExceptionHandler(value = Exception.class)
  //public R<Exception> handlerException(HttpServletRequest req, Exception ex) {
  //  _le(req, ex);
  //  val message = ex.getMessage();
  //  return getExceptionR(ex, message);
  //}
  //
  //public static <T> R<T> restResult(T data, long code, String msg) {
  //  R<T> apiResult = new R<>();
  //  apiResult.setCode(code);
  //  apiResult.setData(data);
  //  apiResult.setMsg(msg);
  //  return apiResult;
  //}
  //
  //private void _le(HttpServletRequest req, Exception ex) {
  //  log.error(String.format("全局控制器异常处理器被执行 %s", ex.getClass().getSimpleName()));
  //}
  //
}

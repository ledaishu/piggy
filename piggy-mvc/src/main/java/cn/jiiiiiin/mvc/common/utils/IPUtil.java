package cn.jiiiiiin.mvc.common.utils;

import cn.jiiiiiin.mvc.common.exception.ApiErrorCode;
import cn.jiiiiiin.mvc.common.exception.ApiException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public final class IPUtil {

  /**
   * 获取服务器端本机IP对象
   * <p>
   * 1、首先尝试获取 `site-local`地址
   * <p>
   * 2、排除本地回环卡
   * <p>
   * 3、如果没有 `site-local`再使用保底方式获取：{@link InetAddress#getLocalHost()}
   *
   * @return {@link InetAddress}
   */
  public static InetAddress getLocalHostExactAddress() {
    try {
      InetAddress candidateAddress = null;
      Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
      while (networkInterfaces.hasMoreElements()) {
        NetworkInterface iface = networkInterfaces.nextElement();
        // 该网卡接口下的ip会有多个，也需要一个个的遍历，找到自己所需要的
        for (Enumeration<InetAddress> inetAddrs = iface.getInetAddresses();
            inetAddrs.hasMoreElements(); ) {
          InetAddress inetAddr = inetAddrs.nextElement();
          // 排除loopback回环类型地址（不管是IPv4还是IPv6 只要是回环地址都会返回true）
          if (!inetAddr.isLoopbackAddress()) {
            if (inetAddr.isSiteLocalAddress()) {
              // 如果是site-local地址，就是它了 就是我们要找的
              // ~~~~~~~~~~~~~绝大部分情况下都会在此处返回你的ip地址值~~~~~~~~~~~~~
              return inetAddr;
            }

            // 若不是site-local地址 那就记录下该地址当作候选
            if (candidateAddress == null) {
              candidateAddress = inetAddr;
            }

          }
        }
      }

      // 如果出去loopback回环地之外无其它地址了，那就回退到原始方案吧
      return candidateAddress == null ? InetAddress.getLocalHost() : candidateAddress;
    } catch (Exception e) {
      throw new ApiException(ApiErrorCode.GET_LOCALHOST_IP_ERR, e);
    }
  }

}

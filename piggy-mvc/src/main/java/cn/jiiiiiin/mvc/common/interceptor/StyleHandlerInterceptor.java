package cn.jiiiiiin.mvc.common.interceptor;

import cn.hutool.core.util.ObjectUtil;
import cn.jiiiiiin.mvc.common.style.Style;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 风格链
 * <p>
 * 文档：https://juejin.cn/post/6844903949502267400
 * <p>
 * 需要动态配置：
 * <p>
 * 参考了： https://blog.csdn.net/qq_28885149/article/details/76559402
 * <p>
 * https://www.iteye.com/blog/elim-1750680
 * <p>
 * TODO 为了给控制器处理之后能传递数据回到chains中，需要定义一个上下文
 *
 * @author jiiiiiin
 */
@Slf4j
@AllArgsConstructor
@Order(Integer.MAX_VALUE)
public class StyleHandlerInterceptor implements HandlerInterceptor {

  private final ApplicationContext applicationContext;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
      val style = ((HandlerMethod) handler).getMethodAnnotation(Style.class);
      if (!ObjectUtil.isNull(style)) {
        // 1. 先校验
        val validates = Style.Util
            .callValidates(request, response, handler, style.chains(), applicationContext);
        // 2. 后转换
        Style.Util.callTransforms(request, response, handler, style.chains(), applicationContext);
        return validates;
      }
    }
    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {
    if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
      val style = ((HandlerMethod) handler).getMethodAnnotation(Style.class);
      if (!ObjectUtil.isNull(style)) {
        Style.Util
            .callHandlers(request, response, handler, style.chains(), applicationContext);
      }
    }
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) throws Exception {
    if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
      val style = ((HandlerMethod) handler).getMethodAnnotation(Style.class);
      if (!ObjectUtil.isNull(style)) {
        Style.Util
            .callCompleter(request, response, handler, style.chains(), applicationContext);
      }
    }
  }
}

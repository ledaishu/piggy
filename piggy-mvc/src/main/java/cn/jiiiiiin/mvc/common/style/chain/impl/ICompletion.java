package cn.jiiiiiin.mvc.common.style.chain.impl;

import cn.jiiiiiin.mvc.common.style.chain.IChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 一般用于对控制器进行资源释放处理
 *
 * 0.bean定义使用{@link cn.jiiiiiin.mvc.common.style.chain.annotaion.Completion}声明
 *
 * 1.一般定义在业务应用的`**.style.command`包下
 *
 * 2.实现{@link IChain#completed(HttpServletRequest, HttpServletResponse, Object)} 接口
 *
 * @author jiiiiiin
 */
public interface ICompletion extends IChain {

}

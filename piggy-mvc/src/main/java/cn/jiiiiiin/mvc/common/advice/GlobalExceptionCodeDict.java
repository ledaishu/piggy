package cn.jiiiiiin.mvc.common.advice;

import lombok.Getter;
import org.springframework.security.access.AccessDeniedException;

/**
 * SS_ 标示 SpringSecurity 抛出的异常；
 *
 * @author jiiiiiin
 */
@Getter
@Deprecated
public enum GlobalExceptionCodeDict {

  SS_ACCESS_DENIED_EXCEPTION(-2000, "没有访问权限错误", AccessDeniedException.class);

  /**
   * 错误码
   */
  private int code;

  /**
   * 描述
   */
  private String desc;

  /**
   * 对应异常类型
   */
  private Class<? extends Exception> aClass;

  GlobalExceptionCodeDict(int code, String desc,
      Class<? extends Exception> aClass) {
    this.code = code;
    this.desc = desc;
    this.aClass = aClass;
  }

}

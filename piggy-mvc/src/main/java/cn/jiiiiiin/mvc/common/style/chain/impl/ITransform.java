package cn.jiiiiiin.mvc.common.style.chain.impl;

import cn.jiiiiiin.mvc.common.style.chain.IChain;

/**
 * 标示为转换处理器
 *
 * @author zhaojun
 */
public interface ITransform extends IChain {
}

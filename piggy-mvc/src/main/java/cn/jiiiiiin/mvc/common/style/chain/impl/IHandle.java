package cn.jiiiiiin.mvc.common.style.chain.impl;

import cn.jiiiiiin.mvc.common.style.chain.IChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 * 一般用于对控制器进行后处理的通用业务逻辑抽象
 * <p>
 * 0.bean定义使用{@link cn.jiiiiiin.mvc.common.style.chain.annotaion.Handler}声明
 * <p>
 * 1.一般定义在业务应用的`**.style.command`包下
 * <p>
 * 2.实现{@link IChain}对应接口
 *
 * @author jiiiiiin
 */
public interface IHandle extends IChain {

}

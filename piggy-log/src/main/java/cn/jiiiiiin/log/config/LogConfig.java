package cn.jiiiiiin.log.config;

import cn.jiiiiiin.log.core.DefLogMDCDataTransport;
import cn.jiiiiiin.log.core.LogMDCDataTransport;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiiiiiin
 */
@Configuration
public class LogConfig {

  /**
   * 业务应用自己实现自己的MDC添加诉求
   *
   * @return {@link LogMDCDataTransport}
   */
  @Bean
  @ConditionalOnMissingBean
  public LogMDCDataTransport logMDCDataTransport() {
    return new DefLogMDCDataTransport();
  }

}

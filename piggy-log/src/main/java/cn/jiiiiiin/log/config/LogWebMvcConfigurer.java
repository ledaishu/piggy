package cn.jiiiiiin.log.config;

import cn.jiiiiiin.log.component.LogAnnotationDataHandlerInterceptor;
import cn.jiiiiiin.log.component.LogAnnotationDataTransportFilter;
import cn.jiiiiiin.log.core.LogMDCDataTransport;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author jiiiiiin
 */
@Configuration
@AllArgsConstructor
public class LogWebMvcConfigurer implements WebMvcConfigurer {

  public final LogMDCDataTransport logMDCDataTransport;

  @Bean
  public FilterRegistrationBean filterRegistrationBean() {
    // 添加记录日志MDC字段的过滤器
    val registration = new FilterRegistrationBean();
    //当过滤器有注入其他bean类时，可直接通过@bean的方式进行实体类过滤器，这样不可自动注入过滤器使用的其他bean类。
    //当然，若无其他bean需要获取时，可直接new CustomFilter()，也可使用getBean的方式。
    registration.setFilter(new LogAnnotationDataTransportFilter(logMDCDataTransport));
    //过滤器名称
    registration.setName("busAnnotationDataTransportFilter");
    //拦截路径
    registration.addUrlPatterns("/*");
    //设置顺序
    registration.setOrder(10);
    return registration;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    // 添加记录日志MDC字段的拦截器
    registry.addInterceptor(new LogAnnotationDataHandlerInterceptor(logMDCDataTransport))
        .addPathPatterns("/*");
  }

}

package cn.jiiiiiin.log.component;

import cn.jiiiiiin.log.core.LogMDCDataTransport;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

/**
 * 业务日志环绕记录器
 *
 * 使用sleuth的annotation概念，记录：sr-ss
 *
 * Annotation:用来及时记录事件的存在，有些重要的注解用来定义请求的开始和结束：
 *
 * cs - Client Request - 客户端发送了一个请求。这个注解描述一个span的开始。
 *
 * sr - Server Received - 服务端接收到了请求，并开始去处理请求。从客户端请求发出后到一个这样的服务端接受事件之间，表示网络延迟时间。
 *
 * ss - Server Sent - 标识为请求处理完成（开始对客户端发送出响应）。从这个时间点到服务端接受到请求之间，表示服务端需要处理请求的时间。
 *
 * cr - Client Received - 表示span的结束。客户端已经从服务端成功的接受到请求了。从这个时间点到服务端响应之间，表示客户端从服务端接受相应需要的时间。
 *
 * @author jiiiiiin
 */
@Slf4j
@AllArgsConstructor
public class LogAnnotationDataTransportFilter implements Filter {

  public final LogMDCDataTransport logMDCDataTransport;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    log.debug("业务日志环绕记录器初始化");
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {
    val request = (HttpServletRequest) servletRequest;
    // 设置字段
    logMDCDataTransport.putRequestFields(request);
    // 必须在设置MDC之后打印日志，否则%X{req_is_debug}将不会生效
    filterChain.doFilter(servletRequest, servletResponse);
    val response = (HttpServletResponse) servletResponse;
    //LogMDCDataTransport.putResponseFields(response);
  }

  @Override
  public void destroy() {
  }
}

package cn.jiiiiiin.log.component;

import cn.jiiiiiin.log.core.LogMDCDataTransport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 业务日志环绕记录器，处理{@link LogAnnotationDataTransportFilter}不能收集到的字段
 *
 * @author jiiiiiin
 */
@Slf4j
@AllArgsConstructor
public class LogAnnotationDataHandlerInterceptor implements HandlerInterceptor {

  public final LogMDCDataTransport logMDCDataTransport;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    // 对于`LogAnnotationDataTransportFilter`不能获取到的数据的一种补充
    logMDCDataTransport.putSpringMVCInterceptorFields(request);
    return true;
  }


  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex)
      throws Exception {
    // 注意：放在这里才生效
    logMDCDataTransport.putResponseFields(response);
  }

}

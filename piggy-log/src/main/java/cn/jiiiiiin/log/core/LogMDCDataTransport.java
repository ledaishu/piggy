package cn.jiiiiiin.log.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 业务个性化日志传输器，将设置的个性化字段记录到slf4j
 * <p>
 * https://github.com/logstash/logstash-logback-encoder#mdc-fields
 *
 * @author jiiiiiin
 */
@SuppressWarnings("ALL")
@Component
public interface LogMDCDataTransport {

  public final static Logger log = LoggerFactory.getLogger(LogMDCDataTransport.class);

  /**
   * 设置sr阶段（请求中对于日志查询和统计具有意义的）MDC字段
   *
   * @param request {@link HttpServletRequest}
   */
  default void putRequestFields(HttpServletRequest request) {
  }

  /**
   * 放置spring mvc拦截器触发时机才能获取到的字段
   *
   * @param request {@link HttpServletRequest}
   */
  default void putSpringMVCInterceptorFields(HttpServletRequest request) {
  }

  /**
   * 设置ss阶段（响应结果中有意义的）MDC字段
   *
   * @param response {@link HttpServletRequest}
   */
  default void putResponseFields(HttpServletResponse response) {
  }
}

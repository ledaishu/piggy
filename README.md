# piggy

个人开发工具集，主要针对springboot项目

```bash
.
├── piggy-data
├── piggy-log
├── piggy-mvc
├── piggy-task-tmpl
├── piggy-validate-code
└── sql
```

### piggy-data

针对开发中和数据的交互工具库

```xml
<!-- https://mvnrepository.com/artifact/cn.jiiiiiin/piggy-data -->
<dependency>
    <groupId>cn.jiiiiiin</groupId>
    <artifactId>piggy-data</artifactId>
    <version>1.0.1</version>
</dependency>
```

### piggy-mvc

针针对开发中涉及控制器相关的工具库

```xml
<!-- https://mvnrepository.com/artifact/cn.jiiiiiin/piggy-mvc -->
<dependency>
    <groupId>cn.jiiiiiin</groupId>
    <artifactId>piggy-mvc</artifactId>
    <version>1.1.2</version>
</dependency>
```

### piggy-log

针对日志相关工具库

```xml
<!-- https://mvnrepository.com/artifact/cn.jiiiiiin/piggy-log -->
<dependency>
    <groupId>cn.jiiiiiin</groupId>
    <artifactId>piggy-log</artifactId>
    <version>1.0.1</version>
</dependency>
```

### piggy-validate-code

针对验证码相关工具库

```xml
<!-- https://mvnrepository.com/artifact/cn.jiiiiiin/piggy-validate-code -->
<dependency>
    <groupId>cn.jiiiiiin</groupId>
    <artifactId>piggy-validate-code</artifactId>
    <version>1.0.2</version>
</dependency>
```

### piggy-task-tmpl

针对后台任务相关工具库

```xml
<!-- https://mvnrepository.com/artifact/cn.jiiiiiin/piggy-task-tmpl -->
<dependency>
    <groupId>cn.jiiiiiin</groupId>
    <artifactId>piggy-task-tmpl</artifactId>
    <version>1.0.3</version>
</dependency>
```

- 需要导入`piggy/piggy-task-tmpl/src/main/resources/sql/schema.sql`目录相关表


